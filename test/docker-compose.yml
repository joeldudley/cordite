#
#   Copyright 2018, Cordite Foundation.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

version: '3.5'

networks:
  cordite:

services:
  corda-db:
    image: postgres:10.5
    ports:
      - "5432:5432"
    volumes: 
      - ./db-init:/docker-entrypoint-initdb.d/
    networks:
      cordite:
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U postgres"]
      interval: 10s
      timeout: 5s
      retries: 5
  network-map:
    image: cordite/network-map:v0.3.1
    ports:
      - "80:80"
    environment:
      - NMS_PORT=80
      - NMS_DB=/mnt/cordite/network-map/db
      - NMS_AUTH_USERNAME=admin
      - NMS_AUTH_PASSWORD=admin
      - NMS_TLS=false
      - NMS_DOORMAN=false
      - NMS_CERTMAN=false
      - NMS_CACHE_TIMEOUT=10S
    networks:
      cordite:
  committee:
    image: ${IMAGE_TAG:-cordite/cordite:edge}
    ports:
      - "8084:8080"
    environment:
      - CORDITE_LEGAL_NAME=O=Cordite Committee, OU=Cordite Foundation, L=London, C=GB
      - CORDITE_P2P_ADDRESS=committee:10002
      - CORDITE_COMPATIBILITY_ZONE_URL=http://network-map
      - CORDITE_FEE_DISPERSAL_CONFIG="{\"feeDispersalRefreshInterval\":1000, \"feeDispersalServicePartyName\":\"Cordite Committee\",\"daoName\":\"Cordite Committee\"}"
      - CORDITE_DB_USER=postgres
      - CORDITE_DB_PASS=postgres
      - CORDITE_DB_DRIVER=org.postgresql.ds.PGSimpleDataSource
      - CORDITE_DB_URL=jdbc:postgresql://corda-db:5432/committee
    depends_on: 
      - "corda-db"
    networks:
      cordite:
  guardian-notary:
    image: ${IMAGE_TAG:-cordite/cordite:edge}
    ports:
       - "8085:8080"
    environment:
      - CORDITE_LEGAL_NAME=O=Cordite Guardian Notary, OU=Cordite Foundation, L=London,C=GB
      - CORDITE_P2P_ADDRESS=guardian-notary:10002
      - CORDITE_COMPATIBILITY_ZONE_URL=http://network-map
      - CORDITE_NOTARY=true
      - CORDITE_DB_USER=postgres
      - CORDITE_DB_PASS=postgres
      - CORDITE_DB_DRIVER=org.postgresql.ds.PGSimpleDataSource
      - CORDITE_DB_URL=jdbc:postgresql://corda-db:5432/guardian
    depends_on: 
      - "corda-db"
    networks:
      cordite:
  metering-notary:
    image: ${IMAGE_TAG:-cordite/cordite:edge}
    ports:
       - "8086:8080"
    environment:
      - CORDITE_LEGAL_NAME=O=Cordite Metering Notary, OU=Cordite Foundation, L=London,C=GB
      - CORDITE_P2P_ADDRESS=metering-notary:10002
      - CORDITE_COMPATIBILITY_ZONE_URL=http://network-map
      - CORDITE_NOTARY=true
      - CORDITE_METERING_CONFIG="{\"meteringRefreshInterval\":1000, \"meteringServiceType\":\"SingleNodeMeterer\", \"daoPartyName\":\"Cordite Committee\", \"meteringNotaryAccountId\":\"metering-account-acc1\", \"meteringPartyName\":\"Cordite Metering Notary\", \"daoName\":\"Cordite Committee\"}"
      - CORDITE_DB_USER=postgres
      - CORDITE_DB_PASS=postgres
      - CORDITE_DB_DRIVER=org.postgresql.ds.PGSimpleDataSource
      - CORDITE_DB_URL=jdbc:postgresql://corda-db:5432/metering
    depends_on: 
      - "corda-db"
    networks:
      cordite:
  bootstrap-notary:
    image: ${IMAGE_TAG:-cordite/cordite:edge}
    ports:
       - "8087:8080"
    environment:
      - CORDITE_LEGAL_NAME=O=Cordite Bootstrap Notary, OU=Cordite Foundation, L=London,C=GB
      - CORDITE_P2P_ADDRESS=bootstrap-notary:10002
      - CORDITE_COMPATIBILITY_ZONE_URL=http://network-map
      - CORDITE_NOTARY=true
      - CORDITE_DB_USER=postgres
      - CORDITE_DB_PASS=postgres
      - CORDITE_DB_DRIVER=org.postgresql.ds.PGSimpleDataSource
      - CORDITE_DB_URL=jdbc:postgresql://corda-db:5432/bootstrap
    depends_on: 
      - "corda-db"
    networks:
      cordite:
  emea:
    image: ${IMAGE_TAG:-cordite/cordite:edge}
    restart: always:0
    ports:
       - "8081:8080"
    environment:
      - CORDITE_LEGAL_NAME=O=Cordite EMEA, OU=Cordite Foundation, L=London,C=GB
      - CORDITE_P2P_ADDRESS=emea:10002
      - CORDITE_COMPATIBILITY_ZONE_URL=http://network-map
      - CORDITE_DB_USER=postgres
      - CORDITE_DB_PASS=postgres
      - CORDITE_DB_DRIVER=org.postgresql.ds.PGSimpleDataSource
      - CORDITE_DB_URL=jdbc:postgresql://corda-db:5432/emea
    depends_on: 
      - "corda-db"
    networks:
      cordite:
  apac:
    image: ${IMAGE_TAG:-cordite/cordite:edge}
    restart: always:0
    ports:
       - "8082:8080"
    environment:
      - CORDITE_LEGAL_NAME=O=Cordite APAC, OU=Cordite Foundation, L=Hong Kong, C=HK
      - CORDITE_P2P_ADDRESS=apac:10002
      - CORDITE_COMPATIBILITY_ZONE_URL=http://network-map
      - CORDITE_DB_USER=postgres
      - CORDITE_DB_PASS=postgres
      - CORDITE_DB_DRIVER=org.postgresql.ds.PGSimpleDataSource
      - CORDITE_DB_URL=jdbc:postgresql://corda-db:5432/apac
    networks:
      cordite:
    depends_on: 
      - "corda-db"
  amer:
    image: ${IMAGE_TAG:-cordite/cordite:edge}
    restart: always:0
    ports:
      - "8083:8080"
    environment:
      - CORDITE_LEGAL_NAME=O=Cordite AMER, OU=Cordite Foundation, L=New York City,C=US
      - CORDITE_P2P_ADDRESS=amer:10002
      - CORDITE_COMPATIBILITY_ZONE_URL=http://network-map
      - CORDITE_DB_USER=postgres
      - CORDITE_DB_PASS=postgres
      - CORDITE_DB_DRIVER=org.postgresql.ds.PGSimpleDataSource
      - CORDITE_DB_URL=jdbc:postgresql://corda-db:5432/amer
    depends_on: 
      - "corda-db"
    networks:
      cordite:
