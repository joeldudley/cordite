.. _daocli:

DAO CLI Demonstration
=====================

.. admonition:: Current node addresses

    -  https://amer-test.cordite.foundation:8080
    -  https://apac-test.cordite.foundation:8080
    -  https://emea-test.cordite.foundation:8080

In this CLI demonstration we will:
- Create a dao
- Add two members in addition to the dao creator
- Create two proposals
- Vote on the proposals
- Accept the proposal which has the vote of all members

.. raw:: html

    <script src="https://asciinema.org/a/wGVOH5LdspA7ZQ4fLJY6jiWKv.js" id="asciicast-wGVOH5LdspA7ZQ4fLJY6jiWKv" async data-theme="solarized-light"></script>

Commands used
-------------

+--------+-------------------------------------------------------------------------------------------------------------+
|  Node  | Command                                                                                                     |
+========+=============================================================================================================+
|  amer  | ``notaries``                                                                                                |
+--------+-------------------------------------------------------------------------------------------------------------+
|  amer  | ``dao.createDao("Famous-Five", "OU=Cordite Foundation, O=Cordite Guardian Notary, L=London, C=GB")``        |
+--------+-------------------------------------------------------------------------------------------------------------+
|  amer  | ``dao.daoInfo("Famous-Five")[0].members``                                                                   |
+--------+-------------------------------------------------------------------------------------------------------------+
|  emea  | ``julian = network.getNodeByLegalName("OU=Cordite Foundation, O=Cordite AMER,                               |
|        | L=New York City, C=US").legalIdentities[0]``                                                                |
+--------+-------------------------------------------------------------------------------------------------------------+
|  emea  | ``proposalState = dao.createNewMemberProposal("Famous-Five", julian)``                                      |
+--------+-------------------------------------------------------------------------------------------------------------+
|  emea  | ``dao.acceptNewMemberProposal(proposalState.proposal.proposalKey, julian)``                                 |
+--------+-------------------------------------------------------------------------------------------------------------+
|  emea  | ``dao.daoInfo("Famous-Five")[0].members``                                                                   |
+--------+-------------------------------------------------------------------------------------------------------------+
|  apac  | ``dick = network.getNodeByLegalName("OU=Cordite Foundation, O=Cordite EMEA,                                 |
|        | L=London, C=GB").legalIdentities[0]``                                                                       |
+--------+-------------------------------------------------------------------------------------------------------------+
|  apac  | ``anneProp = dao.createNewMemberProposal("Famous-Five", dick)``                                             |
+--------+-------------------------------------------------------------------------------------------------------------+
|  amer  | ``ffkey = dao.daoInfo("Famous-Five")[0].daoKey``                                                            |
+--------+-------------------------------------------------------------------------------------------------------------+
|  amer  | ``apkey = dao.newMemberProposalsFor(ffkey)[0].proposal.proposalKey``                                        |
+--------+-------------------------------------------------------------------------------------------------------------+
|  amer  | ``dao.voteForMemberProposal(apkey)```                                                                       |
+--------+-------------------------------------------------------------------------------------------------------------+
|  apac  | ``apkey = anneProp.proposal.proposalKey``                                                                   |
+--------+-------------------------------------------------------------------------------------------------------------+
|  apac  | ``dao.acceptNewMemberProposal(apkey, dick)``                                                                |
+--------+-------------------------------------------------------------------------------------------------------------+
|  apac  | ``dao.daoInfo("Famous-Five")[0].members``                                                                   |
+--------+-------------------------------------------------------------------------------------------------------------+
|  amer  | ``treasureState = dao.createProposal("Treasure Island", "head to treasure island", ffkey)``                 |
+--------+-------------------------------------------------------------------------------------------------------------+
|  apac  | ``ffkey = dao.daoInfo("Famous-Five")[0].daoKey``                                                            |
+--------+-------------------------------------------------------------------------------------------------------------+
|  apac  | ``caravanState = dao.createProposal("Caravan", "go off in a caravan", ffkey)``                              |
+--------+-------------------------------------------------------------------------------------------------------------+
|  emea  | ``ffkey = dao.daoInfo("Famous-Five")[0].daoKey``                                                            |
+--------+-------------------------------------------------------------------------------------------------------------+
|  emea  | ``treasureKey = dao.normalProposalsFor(ffkey)[0].proposal.proposalKey``                                     |
+--------+-------------------------------------------------------------------------------------------------------------+
|  emea  | ``dao.voteForProposal(treasureKey)``                                                                        |
+--------+-------------------------------------------------------------------------------------------------------------+
|  amer  | ``treasureAccept = dao.acceptProposal(treasureState.proposal.proposalKey)``                                 |
+--------+-------------------------------------------------------------------------------------------------------------+
|  amer  | ``treasureAccept.lifecycleState``                                                                           |
+--------+-------------------------------------------------------------------------------------------------------------+