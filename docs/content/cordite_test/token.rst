Token JS Example
================

Cordite has tokens, just like Corda. Cordite however is capable of issuing tokens to 
specific accounts on a node. Token issuance must be notarised. You can create token types 
and accounts to your heart's content!

By this stage you know how to connect using the CLI or the :doc:`Braid JS
Client <braid-js-tutorial>`, so follow along with your
chosen client.

This page explains how to:

- create a token type
- create an account
- issue tokens to an account
- display the balance of an account

Connect to a cordite node
 Firstly, connect to a cordite node - emea test in this case. Create variables for the desired notary, token type name, and account name.

.. code:: js 

    const Proxy = require('braid-client').Proxy;
    const emea = new Proxy({url: 'https://emea-test.cordite.foundation:8080/api/'}, onOpen, onClose, onError, {strictSSL: false})

    let saltedTokenName = 'TOK-'+new Date().getTime()
    let saltedAccountName = 'Acc-'+new Date().getTime()
    let notary = "OU=Cordite Foundation, O=Cordite Guardian Notary, L=London, C=GB"

    function onOpen() {
        console.log("connected to the emea test cordite node")
    }

    function onClose() {
        console.log("closed")
    }

    function onError(err) {
        console.error(err)
    }

Create token type and account
 Now create the token type and the Account. For the ``createTokenType()`` function you must specify a name or
 symbol such as USD, an exponent, and a notary. When creating an account with ``createAccount()``, just specify 
 the desired name/ID and a notary.


.. code:: js

    function onOpen() {
        console.log("connected to the emea test cordite node")

        emea.ledger.createTokenType(saltedTokenName, 2, notary).then( a => {
            console.log("Token with name " + saltedTokenName + " created")
            return emea.ledger.createAccount(saltedAccountName, notary)
        }).then( b => {
            console.log("Account with name " + saltedAccountName + " created")
        }).catch(error => {
            console.error(error)
        })
    }

Issue tokens and check balance
 Finally, issue tokens of the type just created to your new account with ``issueToken()``. Specify the 
 account name, quantity, token type, a message, and a notary. We then query the balance 
 of the account with ``balanceForAccount()`` to check that the tokens were issued. For this function we need only 
 specify the account name.

.. code:: js

    function onOpen() {
        console.log("connected to the emea test cordite node")

        emea.ledger.createTokenType(saltedTokenName, 2, notary).then( a => {
            console.log("Token with name " + saltedTokenName + " created")
            return emea.ledger.createAccount(saltedAccountName, notary)
        }).then( b => {
            console.log("Account with name " + saltedAccountName + " created")
            return emea.ledger.issueToken(saltedAccountName, 100, saltedTokenName, "First issuance", notary)
        }).then( c => {
            console.log("Tokens of type " + saltedTokenName + " issued to " + saltedAccountName)
            return emea.ledger.balanceForAccount(saltedAccountName)
        }).then( d => {
            bal = (d[0].quantity * d[0].displayTokenSize) + " " + d[0].token.symbol
            console.log("Balance for " + saltedAccountName + ": " + bal)
        }).catch(error => {
            console.error(error)
        })
    }

Your console output should now show the balance of your account. The token type and quantity are displayed. For example:

.. code:: sh

    connected to the emea test cordite node
    Token with name TOK-1532429563901 created
    Account with name Acc-1532429563901 created
    Tokens of type TOK-1532429563901 issued to Acc-1532429563901
    Balance for Acc-1532429563901: 100 TOK-1532429563901

The full code is now:

.. code:: js

    const Proxy = require('braid-client').Proxy;
    const emea = new Proxy({url: 'https://emea-test.cordite.foundation:8080/api/'}, onOpen, onClose, onError, {strictSSL: false})

    let saltedTokenName = 'TOK-'+new Date().getTime()
    let saltedAccountName = 'Acc-'+new Date().getTime()
    let notary = "OU=Cordite Foundation, O=Cordite Guardian Notary, L=London, C=GB"

    function onOpen() {
        console.log("connected to the emea test cordite node")

        emea.ledger.createTokenType(saltedTokenName, 2, notary).then( a => {
            console.log("Token with name " + saltedTokenName + " created")
            return emea.ledger.createAccount(saltedAccountName, notary)
        }).then( b => {
            console.log("Account with name " + saltedAccountName + " created")
            return emea.ledger.issueToken(saltedAccountName, 100, saltedTokenName, "First issuance", notary)
        }).then( c => {
            console.log("Tokens of type " + saltedTokenName + " issued to " + saltedAccountName)
            return emea.ledger.balanceForAccount(saltedAccountName)
        }).then( d => {
            bal = (d[0].quantity * d[0].displayTokenSize) + " " + d[0].token.symbol
            console.log("Balance for " + saltedAccountName + ": " + bal)
        }).catch(error => {
            console.error(error)
        })
    }

    function onClose() {
        console.log("closed")
    }

    function onError(err) {
        console.error(err)
    }

