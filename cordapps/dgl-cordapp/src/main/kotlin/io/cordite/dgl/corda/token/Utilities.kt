/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.corda.token

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dgl.corda.account.Account
import io.cordite.dgl.corda.account.AccountAddress
import net.corda.core.contracts.Amount
import net.corda.core.contracts.Issued
import net.corda.core.contracts.PartyAndReference
import net.corda.core.node.ServiceHub
import net.corda.core.node.services.vault.QueryCriteria.VaultCustomQueryCriteria
import net.corda.core.node.services.vault.builder

infix fun <T : Any> T.issuedBy(deposit: PartyAndReference) = Issued(deposit, this)
infix fun <T : Any> Amount<T>.issuedBy(deposit: PartyAndReference) = Amount(quantity, displayTokenSize, token.issuedBy(deposit))

@Suspendable
fun verifyAccountExists(serviceHub: ServiceHub, account: AccountAddress) {
  verifyAccountsExist(serviceHub, listOf(account))
}

@Suspendable
fun verifyAccountsExist(serviceHub: ServiceHub, accounts: List<AccountAddress>) {
  require(accounts.isNotEmpty()) { "no accounts to verify" }
  val accountIds = accounts.map { it.accountId }
  val foundAccounts = serviceHub.vaultService.queryBy(Account.State::class.java, builder {
    VaultCustomQueryCriteria(Account.AccountSchemaV1.PersistentAccount::accountId.`in`(accountIds))
  }).states.map { it.state.data.address.accountId }
  require(foundAccounts.size == accounts.size) { "could not locate ${accountIds.subtract(foundAccounts)}" }
}
