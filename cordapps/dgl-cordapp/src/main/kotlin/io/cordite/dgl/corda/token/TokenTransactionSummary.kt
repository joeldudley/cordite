/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.corda.token

import io.cordite.dgl.corda.account.AccountAddress
import net.corda.core.contracts.Contract
import net.corda.core.contracts.ContractClassName
import net.corda.core.contracts.requireSingleCommand
import net.corda.core.contracts.requireThat
import net.corda.core.crypto.SecureHash
import net.corda.core.identity.AbstractParty
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState
import net.corda.core.serialization.CordaSerializable
import net.corda.core.transactions.LedgerTransaction
import java.time.Instant
import javax.persistence.*

class TokenTransactionSummary : Contract {
  companion object {
    val CONTRACT_ID: ContractClassName = TokenTransactionSummary::class.java.name
  }

  override fun verify(tx: LedgerTransaction) {
    tx.commands.requireSingleCommand<Token.Command>()
    requireThat { "there is exactly one ${TokenTransactionSummary::class.java.name} object in this transaction" using (tx.outputStates.count { it is State } == 1) }
    val transfer = tx.outputsOfType<TokenTransactionSummary.State>().single()
    val groups = tx.groupStates(Token.State::class.java) { it.amount.token }
    for ((inputs, outputs, _) in groups) {
      val debits = inputs.map { NettedAccountAmount(it.accountAddress, -it.amount.quantity, it.amount.token.product) }.asSequence()
      val credits = outputs.map { NettedAccountAmount(it.accountAddress, it.amount.quantity, it.amount.token.product) }.asSequence()
      val nettedGroups = (debits + credits)
          .groupingBy { it.accountAddress to it.tokenType }
          .reduce { _, accumulator, element -> accumulator.copy(quantity = accumulator.quantity + element.quantity) }
          .map { it.value }
          .toSet()
      requireThat { "the netted amounts match" using (nettedGroups == transfer.amounts.toSet()) }
    }
  }

  @CordaSerializable
  data class NettedAccountAmount(val accountAddress: AccountAddress, val quantity: Long, val tokenType: TokenType.Descriptor)

  data class State(
    override val participants: List<AbstractParty>,
    val command: String,
    val amounts: List<NettedAccountAmount>,
    val description: String,
    val transactionTime: Instant = Instant.now(),
    val transactionId: SecureHash? = null
  ) : QueryableState {
    constructor(participants: List<AbstractParty>, command: Token.Command, amounts: List<NettedAccountAmount>, description: String) :
        this(participants, command.javaClass.simpleName, amounts, description)

    override fun generateMappedObject(schema: MappedSchema): PersistentState {
      return TokenTransactionSummarySchemaV1.PersistentTokenTransactionSummary(command, description, transactionTime).apply {
        this.persistentAmounts.addAll(
            amounts.map {
              TokenTransactionSummarySchemaV1.PersistentTokenTransactionAmount(
                  accountId = it.accountAddress.uri,
                  amount = it.quantity,
                  tokenUri = it.accountAddress.uri,
                  transactionTime = transactionTime)
            })
      }
    }

    override fun supportedSchemas(): Iterable<MappedSchema> = listOf(TokenTransactionSummarySchemaV1)
  }

  object TokenTransactionSummarySchema
  object TokenTransactionSummarySchemaV1
    : MappedSchema(
      schemaFamily = TokenTransactionSummarySchema::class.java,
      version = 1,
      mappedTypes = setOf(
          PersistentTokenTransactionSummary::class.java,
          PersistentTokenTransactionAmount::class.java)) {

    @Entity
    @Table(name = "CORDITE_TOKEN_TRANSACTION", indexes = arrayOf(
        Index(name = "operation_idx", columnList = "operation"),
        Index(name = "description_idx", columnList = "description"),
        Index(name = "transaction_summary_time_idx", columnList = "transaction_time")
    ))
    class PersistentTokenTransactionSummary(
        @Column(name = "operation")
        val operation: String,
        @Column(name = "description")
        val description: String,
        @Column(name = "transaction_time")
        val transactionTime: Instant
    ) : PersistentState() {
      @OneToMany(fetch = FetchType.LAZY, cascade = arrayOf(CascadeType.PERSIST))
      @JoinColumns(JoinColumn(name = "transaction_id", referencedColumnName = "transaction_id"), JoinColumn(name = "output_index", referencedColumnName = "output_index"))
      @OrderColumn
      var persistentAmounts: MutableSet<TokenTransactionSummarySchemaV1.PersistentTokenTransactionAmount> = mutableSetOf()
    }

    @Entity
    @Table(name = "CORDITE_TOKEN_TRANSACTION_AMOUNT", indexes = arrayOf(
        Index(name = "account_id_uri_idx", columnList = "account_id_uri"),
        Index(name = "token_uri_idx", columnList = "token_uri"),
        Index(name = "transaction_summary_amount_time_idx", columnList = "transaction_time")
    ))
    class PersistentTokenTransactionAmount(
        @Column(name = "account_id_uri")
        val accountId: String,
        @Column(name = "amount")
        val amount: Long,
        @Column(name = "token_uri")
        val tokenUri: String,
        @Column(name = "transaction_time")
        val transactionTime: Instant
    ) {
      @Id
      @GeneratedValue
      @Column(name = "child_id", unique = true, nullable = false)
      var childId: Int? = null

      @ManyToOne(fetch = FetchType.LAZY)
      @JoinColumns(JoinColumn(name = "transaction_id", referencedColumnName = "transaction_id"), JoinColumn(name = "output_index", referencedColumnName = "output_index"))
      var summary: PersistentTokenTransactionSummary? = null
    }
  }
}