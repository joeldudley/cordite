/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.corda.token.flows

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dgl.corda.account.Account
import io.cordite.dgl.corda.account.AccountAddress
import io.cordite.dgl.corda.token.flows.TransferTokenRecipientFunctions.Companion.checkTokenMoveTransaction
import net.corda.confidential.IdentitySyncFlow
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.FlowSession
import net.corda.core.flows.InitiatedBy
import net.corda.core.flows.SignTransactionFlow
import net.corda.core.transactions.SignedTransaction
import net.corda.core.utilities.unwrap
import java.security.InvalidParameterException

@InitiatedBy(TransferTokenFlow::class)
class TransferTokenRecipientFlow(private val otherSideSession: FlowSession) : FlowLogic<SignedTransaction>() {
  @Suspendable
  override fun call(): SignedTransaction {
    val address = otherSideSession.receive<AccountAddress>().unwrap { it }
    if (!Account.exists(serviceHub, address)) {
      throw InvalidParameterException("${serviceHub.myInfo.legalIdentities.first().name} - unknown account: ${address.accountId}")
    } else {
      otherSideSession.send("OK")
    }
    subFlow(IdentitySyncFlow.Receive(otherSideSession))

    val signTransactionFlow = object : SignTransactionFlow(otherSideSession) {
      override fun checkTransaction(stx: SignedTransaction) = checkTokenMoveTransaction(stx, serviceHub)
    }
    val txId = subFlow(signTransactionFlow).id
    return waitForLedgerCommit(txId)
  }
}