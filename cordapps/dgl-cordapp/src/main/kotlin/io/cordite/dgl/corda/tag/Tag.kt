/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.corda.tag

import net.corda.core.serialization.CordaSerializable

/**
 * Tags are a way of dynamic decoration of certain states in DGL
 */
@CordaSerializable
data class Tag(val category: String, val value: String) {
  companion object {
    const val SEPARATOR = ':'
    fun parse(tag: String) : Tag {
      val parts = tag.split(':')
      if (parts.size != 2) {
        throw RuntimeException("tag is malformed: $tag")
      }
      return Tag(category = parts[0], value = parts[1])
    }
  }

  init {
    if (category.contains(SEPARATOR) || value.contains(SEPARATOR)) {
      throw RuntimeException("tag category and value must not contain the separator ':'")
    }
  }

  override fun toString(): String {
    return "$category:$value"
  }
}

/**
 * WARNING:
 * 1. Be very careful changing the *values* of these as it could break dynamic clients!
 * 2. Strings only!
 */
object WellKnownTagCategories {
  const val DGL_ID = "DGL.ID"
  const val IBAN_ID = "IBAN.ID"
  const val SWIFT_CODE = "SWIFT.CODE"
  const val CURRENCY_CODE = "CURRENCY.CODE"
  const val SETTLEMENT_NETWORK = "SETTLEMENT.NETWORK"
  const val SETTLEMENT_DESTINATION = "SETTLEMENT.DESTINATION"
}

/**
 * WARNING:
 * 1. Be very careful changing the *values* of these as it could break dynamic clients!
 * 2. Strings only!
 */
object WellKnownTagValues {
  const val SETTLEMENT_NETWORK_BTC = "BTC"
  const val SETTLEMENT_NETWORK_ETH = "ETH"
  const val SETTLEMENT_NETWORK_UK_CHAPS = "UK.CHAPS"
  const val SETTLEMENT_NETWORK_UK_FASTERPAYMENTS = "UK.FASTER_PAYMENTS"
  const val SETTLEMENT_NETWORK_EU_SEPA = "EU.SEPA"
}