/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.corda.token

import net.corda.core.contracts.Amount
import net.corda.core.identity.CordaX500Name
import net.corda.testing.core.TestIdentity
import net.corda.testing.node.MockServices
import net.corda.testing.node.ledger
import org.junit.Test
import java.math.BigDecimal

class TokenContractTest {
  private val ledgerServices = MockServices(cordappPackages = listOf("io.cordite.dgl.corda.token"))
  private val firstName = CordaX500Name("theNode", "Bristol", "GB")
  private val firstIdentity = TestIdentity(firstName)
  private val secondName = CordaX500Name("theNode2", "London", "GB")
  private val secondIdentity = TestIdentity(secondName)
  private val tokenType = TokenType.Descriptor("GRG", 2, firstName)

  @Test
  fun `valid Token request has issuer the same as TokenType issuer`(){
    val amount = Amount.fromDecimal(BigDecimal.ONE, tokenType)
    val issuedBy = amount.issuedBy(firstIdentity.ref(1))

    ledgerServices.ledger {
      transaction {
        command(firstIdentity.publicKey, Token.Command.Issue())
        output(Token.CONTRACT_ID, Token.State(issuedBy, listOf(firstIdentity.publicKey), firstIdentity.party, "accountId", "have some dosh"))
        verifies()
      }
    }
  }

  @Test
  fun `Token issuing party cannot be different to TokenType party`(){
    val amount = Amount.fromDecimal(BigDecimal.ONE, tokenType)
    val issuedBy = amount.issuedBy(secondIdentity.ref(1))

    ledgerServices.ledger {
      transaction {
        command(secondIdentity.publicKey, Token.Command.Issue())
        output(Token.CONTRACT_ID, Token.State(issuedBy, listOf(secondIdentity.publicKey), secondIdentity.party, "accountId", "have some dosh"))
        failsWith("Issuer of the token is the same as the TokenType owner")
      }
    }
  }

  @Test
  fun `issue token command should have no inputs`() {
    val amount = Amount.fromDecimal(BigDecimal.ONE, tokenType)
    val issuedBy = amount.issuedBy(firstIdentity.ref(1))

    ledgerServices.ledger {
      transaction {
        command(firstIdentity.publicKey, Token.Command.Issue())
        input(Token.CONTRACT_ID, Token.State(issuedBy, listOf(firstIdentity.publicKey), firstIdentity.party, "accountId", "have some dosh"))
        failsWith("There should be no inputs")
      }
    }
  }

}