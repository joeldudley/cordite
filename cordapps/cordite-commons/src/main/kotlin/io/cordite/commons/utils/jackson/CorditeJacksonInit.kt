/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.commons.utils.jackson

import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.module.SimpleModule
import io.bluebank.braid.corda.serialisation.BraidCordaJacksonInit
import io.vertx.core.json.Json
import net.corda.core.node.services.vault.PageSpecification

class CorditeJacksonInit {
  companion object {
    init {
      BraidCordaJacksonInit.init()
      // TODO: move the following to `braid-corda` module
      val sm = SimpleModule()
          .addSerializer(PageSpecification::class.java, PageSpecificationSerializer())
          .addDeserializer(PageSpecification::class.java, PageSpecificationDeserializer())
      Json.mapper.registerModule(sm)
      Json.prettyMapper.registerModule(sm)
      // TODO: backport this to braid
      Json.mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
      Json.prettyMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
    }
    fun init() {
      // automatically runs the class init once only
    }
  }

}