/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.commons.database

import rx.Observable
import rx.Subscriber
import java.io.InputStream
import java.io.Reader
import java.math.BigDecimal
import java.net.URL
import java.sql.*
import java.sql.Array
import java.sql.Date
import java.util.*

/**
 * returns an observable, that when subscribed to, executes the query
 * and returns a database engine agnostic recordset
 */
fun Connection.executeCaseInsensitiveQuery(query: String): Observable<ResultSet> {
  return Observable.create { subscriber ->
    this.prepareStatement(query).use { preparedStatement ->
      preparedStatement.executeQuery().use { rs ->
        rs.withSubscriber(subscriber)
      }
    }
  }
}

/**
 * Given a fresh, un-iterated, ResultSet, loops the rows, passing a database-agnostic ResultSet
 * to a subscriber
 */
fun ResultSet.withSubscriber(subscriber: Subscriber<in ResultSet>) {
  try {
    val columns = cacheColumnNamesAndIndices()

    while (this.next()) {
      subscriber.onNext(this.toCaseInsensitive(columns))
    }
    subscriber.onCompleted()
  } catch (err: Throwable) {
    subscriber.onError(err)
  }
}

private fun ResultSet.cacheColumnNamesAndIndices(): Map<String, Int> =
  (1..metaData.columnCount).map { idx ->
    metaData.getColumnName(idx).toUpperCase() to idx
  }.toMap()


/**
 * Convert a ResultSet to another ResultSet that is agnostic to column name case
 */
fun ResultSet.toCaseInsensitive(columns: Map<String, Int>): ResultSet {
  return CaseInsensitiveResultSet(this, columns)
}

/**
 * Wrapper for ResultSet that acts as a case-insensitive column name adapter
 */
class CaseInsensitiveResultSet(private val resultSet: ResultSet, private val columns: Map<String, Int>) : ResultSet by resultSet {
  override fun getString(columnLabel: String): String {
    return getString(findColumn(columnLabel))
  }

  override fun getInt(columnLabel: String): Int {
    return getInt(findColumn(columnLabel))
  }

  override fun getArray(columnLabel: String): Array {
    return getArray(findColumn(columnLabel))
  }

  override fun getBigDecimal(columnLabel: String): BigDecimal {
    return getBigDecimal(findColumn(columnLabel))
  }

  override fun getFloat(columnLabel: String): Float {
    return getFloat(findColumn(columnLabel))
  }

  override fun getDouble(columnLabel: String): Double {
    return getDouble(findColumn(columnLabel))
  }

  override fun getLong(columnLabel: String): Long {
    return getLong(findColumn(columnLabel))
  }

  override fun getBoolean(columnLabel: String): Boolean {
    return getBoolean(findColumn(columnLabel))
  }

  override fun getShort(columnLabel: String): Short {
    return getShort(findColumn(columnLabel))
  }

  override fun getAsciiStream(columnLabel: String): InputStream {
    return getAsciiStream(findColumn(columnLabel))
  }

  override fun getBinaryStream(columnLabel: String): InputStream {
    return getBinaryStream(findColumn(columnLabel))
  }

  override fun getBlob(columnLabel: String): Blob {
    return getBlob(findColumn(columnLabel))
  }

  override fun getByte(columnLabel: String): Byte {
    return getByte(findColumn(columnLabel))
  }

  override fun getBytes(columnLabel: String?): ByteArray {
    return getBytes(findColumn(columnLabel))
  }

  override fun getCharacterStream(columnLabel: String?): Reader {
    return getCharacterStream(findColumn(columnLabel))
  }

  override fun getClob(columnLabel: String?): Clob {
    return getClob(findColumn(columnLabel))
  }

  override fun getDate(columnLabel: String?): Date {
    return getDate(findColumn(columnLabel))
  }

  override fun getDate(columnLabel: String?, cal: Calendar?): Date {
    return getDate(findColumn(columnLabel), cal)
  }

  override fun getObject(columnLabel: String?): Any {
    return getObject(findColumn(columnLabel))
  }

  override fun getNCharacterStream(columnLabel: String?): Reader {
    return getNCharacterStream(findColumn(columnLabel))
  }

  override fun getNClob(columnLabel: String?): NClob {
    return getNClob(findColumn(columnLabel))
  }

  override fun getObject(columnLabel: String?, map: MutableMap<String, Class<*>>?): Any {
    return getObject(findColumn(columnLabel), map)
  }

  override fun getNString(columnLabel: String?): String {
    return getNString(findColumn(columnLabel))
  }

  override fun getRef(columnLabel: String?): Ref {
    return getRef(findColumn(columnLabel))
  }

  override fun <T : Any?> getObject(columnLabel: String?, type: Class<T>?): T {
    return getObject(findColumn(columnLabel), type)
  }

  override fun getRowId(columnLabel: String?): RowId {
    return getRowId(findColumn(columnLabel))
  }

  override fun getSQLXML(columnLabel: String?): SQLXML {
    return getSQLXML(findColumn(columnLabel))
  }

  override fun getTime(columnLabel: String?): Time {
    return getTime(findColumn(columnLabel))
  }

  override fun getTime(columnLabel: String?, cal: Calendar?): Time {
    return getTime(findColumn(columnLabel), cal)
  }

  override fun getTimestamp(columnLabel: String?): Timestamp {
    return getTimestamp(columnLabel)
  }

  override fun getTimestamp(columnLabel: String?, cal: Calendar?): Timestamp {
    return getTimestamp(findColumn(columnLabel), cal)
  }

  override fun getURL(columnLabel: String?): URL {
    return getURL(findColumn(columnLabel))
  }

  override fun findColumn(columnLabel: String?): Int {
    if (columnLabel == null) {
      throw NullPointerException("columnLabel must not be null")
    }
    return columns[columnLabel.toUpperCase()] ?: throw SQLException("cannot find column $columnLabel")
  }
}