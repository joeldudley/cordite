/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.contract

import io.cordite.dgl.corda.token.TokenType
import io.cordite.metering.daostate.MeteringFeeAllocator
import io.cordite.metering.schema.MeteringInvoiceSplitSchemaV1
import net.corda.core.contracts.Contract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.internal.x500Name
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState
import net.corda.core.serialization.CordaSerializable
import java.time.Instant
import java.util.*

/**
 * Please note: we are using the concept of ownable state here, without using ownable state as we don't want the move command...
 * we can't arbitrarily move the MeteringInvoice between parties - the flow is strict for which party can do what
 */
data class MeteringInvoiceSplitState(val meteringInvoiceSplitProperties: MeteringInvoiceSplitProperties, val owner: AbstractParty) : QueryableState, LinearState {

  val contract: Contract = MeteringInvoiceContract()
  override val participants: List<AbstractParty> = listOf(owner)
  override val linearId: UniqueIdentifier =UniqueIdentifier(meteringInvoiceSplitProperties.meteredTransactionId)

  override fun generateMappedObject(schema: MappedSchema): PersistentState {
    return when (schema) {
      is MeteringInvoiceSplitSchemaV1 -> MeteringInvoiceSplitSchemaV1.PersistentMeteringInvoiceSplit(
        meteredTransactionId = meteringInvoiceSplitProperties.meteredTransactionId,

        // 186 - use full token descriptor properties in place of just the Ccy string
        // symbol = meteringInvoiceProperties.symbol,
        tokenTypeSymbol = meteringInvoiceSplitProperties.tokenDescriptor.symbol,
        tokenTypeExponent = meteringInvoiceSplitProperties.tokenDescriptor.exponent,
        tokenTypeIssuer = meteringInvoiceSplitProperties.tokenDescriptor.issuerName.toString(),

        amount = meteringInvoiceSplitProperties.amount,
        splitInvoiceId = meteringInvoiceSplitProperties.splitInvoiceId,
        meteringSplitState = meteringInvoiceSplitProperties.meteringSplitState.name,
        invoicedParty = meteringInvoiceSplitProperties.invoicedParty.name.toString(),
        invoicingNotary = meteringInvoiceSplitProperties.invoicingNotary.name.toString(),
        daoParty = meteringInvoiceSplitProperties.daoParty.name.toString(),
        finalAccountId = meteringInvoiceSplitProperties.finalAccountId,
        finalParty = meteringInvoiceSplitProperties.finalParty.name.toString(),
        owner = owner.nameOrNull()?.x500Name?.toString() ?: "",
        createdDateTime = meteringInvoiceSplitProperties.createdDateTime)
      else -> throw IllegalArgumentException("Unrecognised schema $schema")
    }
  }
  override fun supportedSchemas(): Iterable<MappedSchema> = listOf(MeteringInvoiceSplitSchemaV1)
}

@CordaSerializable
  data class MeteringInvoiceSplitProperties(
    val meteredTransactionId : String,
    val tokenDescriptor: TokenType.Descriptor,
    val amount : Int,
    val allocations: MeteringFeeAllocator.FeesToDisperse,
    val splitInvoiceId: String = UUID.randomUUID().toString(),
    val meteringSplitState: MeteringSplitState,
    val invoicedParty : Party,
    val invoicingNotary : Party,
    val daoParty : Party,
    val finalAccountId : String,
    val finalParty : Party,
    val createdDateTime: Instant
)

@CordaSerializable
    enum class MeteringSplitState {
    SPLIT_ISSUED,
    SPLIT_DISPERSED
  }
