/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.schema

import io.cordite.metering.contract.MeteringSplitState
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import java.time.Instant
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Index
import javax.persistence.Table

object MeteringInvoiceSplitSchema

object MeteringInvoiceSplitSchemaV1 : MappedSchema(schemaFamily = MeteringInvoiceSplitSchema.javaClass, version = 1, mappedTypes = listOf(PersistentMeteringInvoiceSplit::class.java)) {

  //TODO - add party constraint in here and replace split_invoice_id with metering_transaction_id  https://gitlab.com/cordite/cordite/issues/269
  //
  @Entity
  @Table(name = "Cordite_Metering_Invoice_Split",
    indexes = arrayOf(Index(name = "split_invoice_id", columnList = "split_invoice_id,metering_split_state", unique = false)))
  class PersistentMeteringInvoiceSplit(
    @Column(name = "metered_transaction_id", nullable = false)
    var meteredTransactionId: String,
    @Column(name = "token_type_symbol", nullable = false)
    var tokenTypeSymbol: String,
    @Column(name = "token_type_exponent", nullable = false)
    var tokenTypeExponent: Int,
    @Column(name = "token_type_issuer", nullable = false)
    var tokenTypeIssuer: String, // x.500 name as string

    @Column(name = "amount", nullable = false)
    var amount: Int,
    @Column(name = "split_invoice_id", nullable = false)
    var splitInvoiceId: String,
    @Column(name = "metering_split_state", nullable = false)
    var meteringSplitState: String,
    @Column(name = "invoiced_party", nullable = false)
    var invoicedParty: String,
    @Column(name = "invoicing_notary", nullable = false)
    var invoicingNotary: String,
    @Column(name = "dao_party", nullable = false)
    var daoParty: String,
    @Column(name = "final_account_id", nullable = false)
    var finalAccountId: String,
    @Column(name = "final_party", nullable = false)
    var finalParty: String,
    @Column(name = "owner", nullable = false)
    var owner: String,
    @Column(name = "created_date_time", nullable = false)
    var createdDateTime : Instant?
  ) : PersistentState() {
    //UGH - the following code is needed to support vault querying - but it's pretty ugly
    constructor() : this("", "",0,"", 0, "", MeteringSplitState.SPLIT_ISSUED.toString(), "", "", "", "", "","", null)
  }
}
