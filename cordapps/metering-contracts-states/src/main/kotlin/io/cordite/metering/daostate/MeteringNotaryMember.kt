/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.daostate

import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.serialization.CordaSerializable

/*
The Flow:
Dao Member Proposes for MeteringNotary to Join
All Members Vote
On Approval Metering Notary Member is Added to Dao
Metering Service gets the Dao status using a flow - need to work out how often this is
 */
@CordaSerializable
data class MeteringNotaryMember(
  val notaryParty : Party,
  val meteringClusterName : CordaX500Name?,
  val accountId : String,
  val description : String,
  val meteringNotaryType : MeteringNotaryType)

@CordaSerializable
enum class MeteringNotaryType {
  METERER,
  GUARDIAN
}

