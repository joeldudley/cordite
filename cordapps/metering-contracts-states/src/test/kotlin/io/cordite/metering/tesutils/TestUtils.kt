/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.tesutils

import io.cordite.dgl.corda.token.TokenType
import net.corda.core.identity.Party

class TestUtils {

  companion object {

    fun TestTokenDescriptor(issuer: Party ) : TokenType.Descriptor {
      return TokenType.Descriptor("XTS", 2, issuer.name);
    }
    fun TestTokenDescriptor(issuer: Party, symbol: String="XTS" ) : TokenType.Descriptor {
      return TokenType.Descriptor(symbol, 2, issuer.name);
    }
    fun TestTokenDescriptor(issuer: Party, symbol: String, exponent: Int) : TokenType.Descriptor {
      return TokenType.Descriptor(symbol, exponent, issuer.name);
    }
  }

}