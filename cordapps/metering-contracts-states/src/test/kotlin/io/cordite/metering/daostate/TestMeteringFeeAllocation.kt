/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.daostate

import org.junit.Test

class TestMeteringFeeAllocation{

  @Test
  fun `we can allocated nicely rounded fees`(){

    val meteringFeeAllocation = MeteringFeeAllocation("blahAccount",50,40,10, "dfa" )
    val allocatedFees = MeteringFeeAllocator.AllocateFeesFromAmount(30,meteringFeeAllocation)

    assert(allocatedFees.daoFoundationAllocation == 15)
    assert(allocatedFees.meterNotaryAllocation == 12)
    assert(allocatedFees.guardianNotaryAllocation == 3)
  }

  @Test
  fun `fraction allocations and round ups just work out man`(){

    val meteringFeeAllocation = MeteringFeeAllocation("blahAccount",33,45,13, "dfa")
    val allocatedFees = MeteringFeeAllocator.AllocateFeesFromAmount(10,meteringFeeAllocation)

    assert(allocatedFees.daoFoundationAllocation == 3)
    assert(allocatedFees.meterNotaryAllocation == 6)
    assert(allocatedFees.guardianNotaryAllocation == 1)
  }

  @Test
  fun `we can handle zeros cos we are hard core devs`(){

    val meteringFeeAllocation = MeteringFeeAllocation("blahAccount",50,50,0,"deathfromabove")
    val allocatedFees = MeteringFeeAllocator.AllocateFeesFromAmount(10,meteringFeeAllocation)

    assert(allocatedFees.daoFoundationAllocation == 5)
    assert(allocatedFees.meterNotaryAllocation == 5)
    assert(allocatedFees.guardianNotaryAllocation == 0)

    val allocatedFeesII = MeteringFeeAllocator.AllocateFeesFromAmount(0,meteringFeeAllocation)
    assert(allocatedFeesII.daoFoundationAllocation == 0)
    assert(allocatedFeesII.meterNotaryAllocation == 0)
    assert(allocatedFeesII.guardianNotaryAllocation == 0)
  }
}