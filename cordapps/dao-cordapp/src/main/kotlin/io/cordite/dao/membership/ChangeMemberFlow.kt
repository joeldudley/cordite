/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.membership

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dao.data.DataHelper
import io.cordite.dao.core.DaoContract
import io.cordite.dao.core.DaoState
import io.cordite.dao.proposal.ProposalKey
import io.cordite.dao.proposal.ProposalLifecycle
import io.cordite.dao.proposal.ProposalState
import io.cordite.commons.utils.contextLogger
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.transactions.SignedTransaction

@InitiatingFlow
class ChangeMemberFlow(private val proposal: MemberProposal) : FlowLogic<MembershipState>() {

  companion object {
    private val log = contextLogger()
  }

  @Suspendable
  override fun call(): MembershipState {
    val me = serviceHub.myInfo.legalIdentities.first()
    val daoName = proposal.daoName

    val proposalStateAndRef = getAndCheckProposalState(proposal.proposalKey)
    val existingDaoTxStateAndRef = daoState(daoName)
    val existingMembershipTxStateAndRef = DataHelper.membershipStateAndRefFor(existingDaoTxStateAndRef.state.data.daoKey, serviceHub)
    val notary = existingDaoTxStateAndRef.state.notary

    val (outputMemberState, txBuilder) = MembershipContract.generateChangeMember(existingMembershipTxStateAndRef, proposalStateAndRef, notary)
    DaoContract.generateChangeMember(existingDaoTxStateAndRef, proposal.member, proposalStateAndRef.state.data.proposal.type, txBuilder)

    txBuilder.verify(serviceHub)

    val signedTx = serviceHub.signInitialTransaction(txBuilder)
    val signers = existingDaoTxStateAndRef.state.data.members + outputMemberState.members
    val flowSessions = signers.filter { it != me }.map { initiateFlow(it) }

    val fullySignedUnNotarisedTx = subFlow(CollectSignaturesFlow(signedTx, flowSessions, CollectSignaturesFlow.tracker()))

    val finalTx = subFlow(FinalityFlow(fullySignedUnNotarisedTx))
    log.info("$proposal applied to $daoName in tx $finalTx")

    return outputMemberState
  }

  private fun daoState(daoName: String): StateAndRef<DaoState> {
    val vaultPage = serviceHub.vaultService.queryBy(DaoState::class.java, QueryCriteria.LinearStateQueryCriteria(externalId = listOf(daoName)))
    if (vaultPage.states.size != 1) {
      log.warn("incorrect number of DAOs with name: $daoName found (${vaultPage.totalStatesAvailable}")
      throw IllegalStateException("there should only be one Dao with a given name")
    }

    return vaultPage.states.first()
  }

  private fun getAndCheckProposalState(proposalKey: ProposalKey): StateAndRef<ProposalState<MemberProposal>> {
    val vaultPage = serviceHub.vaultService.queryBy(ProposalState::class.java, QueryCriteria.LinearStateQueryCriteria(linearId = listOf(proposalKey.uniqueIdentifier)))
    if (vaultPage.states.size != 1) {
      log.warn("incorrect number of unspent proposal states with key: $proposalKey found (${vaultPage.totalStatesAvailable}")
      throw IllegalStateException("incorrect number of unspent proposal states with key: $proposalKey found (${vaultPage.totalStatesAvailable}")
    }
    @Suppress("UNCHECKED_CAST")
    val stateAndRef = vaultPage.states.first() as StateAndRef<ProposalState<MemberProposal>>
    val proposalState = stateAndRef.state.data

    if (proposalState.lifecycleState != ProposalLifecycle.ACCEPTED) {
      log.warn("proposal state for $proposalKey is not ACCEPTED")
      throw IllegalStateException("proposal state for $proposalKey is not ACCEPTED")
    }

    return stateAndRef
  }

}

@InitiatedBy(ChangeMemberFlow::class)
class ChangeMemberFlowResponder(val proposerSession: FlowSession) : FlowLogic<Unit>() {

  companion object {
    private val log = contextLogger()
  }

  @Suspendable
  override fun call() {

    val signTransactionFlow = object : SignTransactionFlow(proposerSession, SignTransactionFlow.tracker()) {
      override fun checkTransaction(stx: SignedTransaction) = requireThat {
        // the tx has already been verified so we actually have nothing to check here unless we are the member
        val commands = stx.toLedgerTransaction(serviceHub, false).commandsOfType(MembershipContract.Commands.AddMember::class.java)
        commands.filter { serviceHub.myInfo.legalIdentities.contains(it.value.newMember) }.forEach {
          log.info("logically check that i want to be a new member of the dao (in the output dao state")
        }
      }
    }

    val stx = subFlow(signTransactionFlow)

    waitForLedgerCommit(stx.id)
  }

}