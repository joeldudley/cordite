/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.proposal

import co.paralleluniverse.fibers.Suspendable
import net.corda.core.contracts.CommandWithParties
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonTypeInfo
import io.cordite.dao.core.DaoKey
import io.cordite.dao.core.ModelData
import io.cordite.dao.core.ChangeModelDataFlow
import io.cordite.dao.core.DaoState
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.Requirements.using
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FlowLogic
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.serialization.CordaSerializable
import java.util.*

@CordaSerializable
data class ProposalKey(val name: String, val uuid: UUID = UUID.randomUUID()) {
  val uniqueIdentifier: UniqueIdentifier = UniqueIdentifier(name, uuid)
}

@CordaSerializable
enum class ProposalLifecycle {
  OPEN, ACCEPTED, REJECTED
}

@JsonIgnoreProperties("linearId", "participants", "name")
@CordaSerializable
data class ProposalState<T : Proposal>(val proposal: T, val proposer: Party, val supporters: Set<Party>, val members: Set<Party>, val daoKey: DaoKey, val lifecycleState: ProposalLifecycle = ProposalLifecycle.OPEN) : LinearState {

  val name = proposal.key().name

  override val participants: List<AbstractParty> = (members + supporters).toList()

  override val linearId: UniqueIdentifier
    get() = proposal.key().uniqueIdentifier

  fun copyWithNewSupporter(newSupporter: Party): ProposalState<T> {
    return ProposalState(proposal, proposer, supporters + newSupporter, members, daoKey, lifecycleState)
  }

  fun copyWithoutSupporter(noLongerSupporter: Party): ProposalState<T> {
    return ProposalState(proposal, proposer, supporters - noLongerSupporter, members, daoKey, lifecycleState)
  }

  fun copyWithNewMember(newMember: Party): ProposalState<T> {
    return ProposalState(proposal, proposer, supporters, members + newMember, daoKey, lifecycleState)
  }

  fun copyReplacingMembers(newMembers: Set<Party>): ProposalState<T> {
    return ProposalState(proposal, proposer, supporters.intersect(newMembers), newMembers, daoKey, lifecycleState)
  }

  fun copyWithNewLifecycleState(lifecycleState: ProposalLifecycle): ProposalState<T> {
    return ProposalState(proposal, proposer, supporters, members, daoKey, lifecycleState)
  }

  fun isValid(percentage: Double): Boolean {
    return supporters.size.toDouble() / members.size >= percentage
  }

  // TODO: wondering whether these checks really belong here? could split up and have the tests here and the require that bits in the contract? https://gitlab.com/cordite/cordite/issues/279
  fun checkSignersForProposal(command: CommandWithParties<ProposalContract.Commands>) {
    "proposer must be signer" using (command.signers.contains(proposer.owningKey))
    proposal.verify(this)
    "members must be signers" using (command.signers.containsAll(members.map { it.owningKey }))
  }

  fun participantSet() = (members + supporters).toSet()

}

@JsonTypeInfo(use= JsonTypeInfo.Id.CLASS, include= JsonTypeInfo.As.PROPERTY, property="@class")
@CordaSerializable
interface Proposal {
  fun key(): ProposalKey
  fun verifyCreate(state: ProposalState<*>)
  fun verify(state: ProposalState<*>)
  fun initialSupporters(proposer: Party): Set<Party>
  @Suspendable
  fun handleAcceptanceOnAcceptor(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>, notary: Party)
  @Suspendable
  fun handleAcceptanceAllButAcceptingMembers(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>, notary: Party)

}

@CordaSerializable
data class NormalProposal(val name: String, val description: String, val proposalKey: ProposalKey = ProposalKey(name)) : Proposal {

  override fun key() = proposalKey

  override fun verifyCreate(state: ProposalState<*>) {
    "proposer must be only initial supporter" using (state.supporters == setOf(state.proposer))
  }

  override fun verify(state: ProposalState<*>) {
    "supporters must be members" using (state.members.containsAll(state.supporters))
  }

  override fun initialSupporters(proposer: Party): Set<Party> {
    return setOf(proposer)
  }

  @Suspendable
  override fun handleAcceptanceOnAcceptor(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>, notary: Party) {
    // not needed...
  }

  @Suspendable
  override fun handleAcceptanceAllButAcceptingMembers(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>, notary: Party) {
    // not needed...
  }

}



