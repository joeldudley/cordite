/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao


import io.cordite.commons.utils.contextLogger
import io.cordite.commons.utils.toVertxFuture
import io.cordite.commons.utils.transaction
import io.cordite.dao.core.*
import io.cordite.dao.data.DataHelper
import io.cordite.dao.economics.IssuanceProposal
import io.cordite.dao.membership.MemberProposal
import io.cordite.dao.membership.MembershipKey
import io.cordite.dao.membership.MembershipModelData
import io.cordite.dao.membership.MembershipState
import io.cordite.dao.proposal.*
import io.cordite.dgl.corda.token.TokenType
import io.vertx.core.Future
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.node.AppServiceHub
import net.corda.core.node.services.AttachmentId
import net.corda.core.node.services.trackBy
import rx.Observable

interface DaoApi {
  // in principle you _could_ have two DAOs with the same name and different create times...
  // need to come back and fix this...but it's nicer than returning null...
  fun daoInfo(daoName: String): List<DaoState>
  fun daoFor(daoKey: DaoKey): DaoState
  fun listDaos(): List<DaoState>
  fun listenForDaoUpdates(): Observable<DaoState>

  fun membershipStateFor(daoKey: DaoKey): MembershipState

  fun createDao(daoName: String, notaryName: CordaX500Name): Future<DaoState>
  fun createDao(daoName: String, minimumMemberCount: Int, strictMode: Boolean, notaryName: CordaX500Name): Future<DaoState>
  // NB THIS IS AN ARRAY DUE TO A JACKSON/KOTLIN ISSUE - will go back to being a Set again later
  fun createDao(daoName: String, initialModelData: Array<ModelData>, notaryName: CordaX500Name): Future<DaoState>

  fun listProposalKeys(): List<ProposalKey>
  fun proposalFor(proposalKey: ProposalKey): ProposalState<out Proposal>
  fun listenForProposalUpdates(): Observable<ProposalState<out Proposal>>

  fun <T: Proposal> createProposal(proposal: T, daoKey: DaoKey): Future<ProposalState<T>>
  fun voteForProposal(proposalKey: ProposalKey): Future<Unit>
  fun acceptProposal(proposalKey: ProposalKey): Future<ProposalLifecycle>

  fun <T: Proposal> sponsorCreateProposal(proposal: T, daoName: String, sponsor: CordaX500Name): Future<ProposalState<T>>
  fun sponsorAcceptProposal(proposalKey: ProposalKey, daoKey: DaoKey, sponsor: CordaX500Name): Future<ProposalLifecycle>

  fun normalProposalsFor(daoKey: DaoKey): List<ProposalState<NormalProposal>>
  fun createNormalProposal(name: String, description: String, daoKey: DaoKey): Future<ProposalState<NormalProposal>>

  fun newMemberProposalsFor(daoKey: DaoKey): List<ProposalState<MemberProposal>>
  fun createNewMemberProposal(daoName: String, sponsor: CordaX500Name): Future<ProposalState<MemberProposal>>
  fun createRemoveMemberProposal(daoKey: DaoKey, member: CordaX500Name): Future<ProposalState<MemberProposal>>

  fun requestProposalConsistencyCheckFor(daoKey: DaoKey): Future<Set<ProposalKey>>

  fun modelDataProposalsFor(daoKey: DaoKey): List<ProposalState<ModelDataProposal>>
  fun createModelDataProposal(name: String, newModelData: ModelData, daoKey: DaoKey): Future<ProposalState<ModelDataProposal>>

  fun createIssuanceProposal(tokenType: TokenType.Descriptor, amount: String, daoKey: DaoKey): Future<ProposalState<IssuanceProposal>>

  // TOFO shift these into braid?  or atleast into a node info service
  fun nodeConfig(): String
  fun whitelist(): Map<String, List<AttachmentId>>
  fun listNodes(): List<CordaX500Name>
}

class DaoApiImpl(val serviceHub: AppServiceHub) : DaoApi {

  companion object {
    private val log = contextLogger()
  }

  private val me = serviceHub.myInfo.legalIdentities.first()

  override fun nodeConfig(): String {
    val sb = StringBuilder()
    sb.appendln("MY NODE INFO:").appendln("--------------------")
    sb.appendln(serviceHub.myInfo)
    sb.appendln("WHITELIST:").appendln("--------------------")
    serviceHub.networkParameters.whitelistedContractImplementations.forEach{
      sb.appendln(it.key)
      it.value.forEach { sb.append("\t").appendln(it) }
    }
    sb.appendln("NOTARIES:").appendln("--------------------")
    serviceHub.networkParameters.notaries.forEach { sb.append(it) }
    log.info(sb.toString())
    return sb.toString()
  }

  override fun whitelist(): Map<String, List<AttachmentId>> {
    return serviceHub.networkParameters.whitelistedContractImplementations
  }

  // in principle you _could_ have two DAOs with the same name and different create times...
  // need to come back and fix this...but it's nicer than returning null...
  override fun daoInfo(daoName: String): List<DaoState> {
    return serviceHub.transaction {
      DataHelper.daoStatesFor(daoName, serviceHub)
    }
  }

  override fun daoFor(daoKey: DaoKey): DaoState {
    return serviceHub.transaction {
      DataHelper.daoStateFor(daoKey, serviceHub)
    }
  }

  override fun listDaos(): List<DaoState> {
    return serviceHub.transaction {
      DataHelper.daoStates(serviceHub)
    }
  }

  override fun listenForDaoUpdates(): Observable<DaoState> {
    return serviceHub.transaction {
      serviceHub.vaultService.trackBy<DaoState>().updates
          .map { update ->
            update.produced.single { it.state.data is DaoState }.state.data
          }
          .doOnNext { log.info("DaoUpdateListener: DaoState ${it.name} updated") }
          .doOnError { log.error("Error while listening for DaoState updates: $it") }
    }
  }

  override fun listenForProposalUpdates(): Observable<ProposalState<out Proposal>> {
    return serviceHub.transaction {
      serviceHub.vaultService.trackBy<ProposalState<out Proposal>>().updates
          .map { update ->
            update.produced.single { it.state.data is ProposalState<out Proposal> }.state.data
          }
          .doOnNext { log.info("ProposalUpdateListener: DaoState ${it.name} updated") }
          .doOnError { log.error("Error while listening for ProposalState updates: $it") }
    }
  }

  override fun listProposalKeys(): List<ProposalKey> {
    return serviceHub.transaction {
      DataHelper.proposalKeys(serviceHub)
    }
  }

  override fun proposalFor(proposalKey: ProposalKey): ProposalState<out Proposal> {
    return serviceHub.transaction {
      DataHelper.proposalFor(proposalKey, serviceHub)
    }
  }

  override fun <T: Proposal> createProposal(proposal: T, daoKey: DaoKey): Future<ProposalState<T>> {
    log.info("creating proposal: $proposal")
    val flowFuture = serviceHub.startFlow(CreateProposalFlow(proposal, daoKey)).returnValue
    return flowFuture.toVertxFuture()
  }

  override fun voteForProposal(proposalKey: ProposalKey): Future<Unit> {
    log.info("voting for proposal: $proposalKey")
    val flowFuture = serviceHub.startFlow(VoteForProposalFlow(proposalKey)).returnValue
    return flowFuture.toVertxFuture()
  }

  override fun acceptProposal(proposalKey: ProposalKey): Future<ProposalLifecycle> {
    log.info("accepting proposal: $proposalKey")

    val flowFuture = serviceHub.startFlow(AcceptProposalFlow(proposalKey)).returnValue
    return flowFuture.toVertxFuture()
  }

  override fun <T : Proposal> sponsorCreateProposal(proposal: T, daoName: String, sponsor: CordaX500Name): Future<ProposalState<T>> {
    log.info("asking $sponsor to prorpose $proposal for me: ${serviceHub.myInfo.legalIdentities.first().name}")
    val sponsorParty = findNode(sponsor)
    val flowFuture = serviceHub.startFlow(SponsorProposalFlow(CreateSponsorAction(proposal, daoName), sponsorParty)).returnValue
    return flowFuture.toVertxFuture()
  }

  override fun sponsorAcceptProposal(proposalKey: ProposalKey, daoKey: DaoKey, sponsor: CordaX500Name): Future<ProposalLifecycle> {
    log.info("attempting to accept proposal $proposalKey, sponsored by $sponsor")
    val sponsorParty = findNode(sponsor)
    val flowFuture = serviceHub.startFlow(SponsorProposalFlow(AcceptSponsorAction(proposalKey), sponsorParty)).returnValue
    return flowFuture.toVertxFuture()
  }

  override fun membershipStateFor(daoKey: DaoKey): MembershipState {
    return serviceHub.transaction {
      DataHelper.membershipStateFor(daoKey, serviceHub)
    }
  }

  override fun createDao(daoName: String, notaryName : CordaX500Name): Future<DaoState> {
    log.info("creating new dao: $daoName")
    val notary = findNotary(notaryName)
    return serviceHub.startFlow(CreateDaoFlow(daoName, notary, emptySet())).returnValue.toVertxFuture()
  }

  override fun createDao(daoName: String, minimumMemberCount: Int, strictMode: Boolean, notaryName: CordaX500Name): Future<DaoState> {
    return createDao(daoName, arrayOf(MembershipModelData(MembershipKey(daoName), minimumMemberCount, strictMode = strictMode)), notaryName)
  }

  override fun createDao(daoName: String, initialModelData: Array<ModelData>, notaryName: CordaX500Name): Future<DaoState> {
    log.info("creating new dao: $daoName")
    val notary = findNotary(notaryName)
    return serviceHub.startFlow(CreateDaoFlow(daoName, notary, initialModelData.toSet())).returnValue.toVertxFuture()
  }

  // surely we can refactor the below...
  override fun newMemberProposalsFor(daoKey: DaoKey): List<ProposalState<MemberProposal>> {
    return serviceHub.transaction {
      DataHelper.proposalStatesFor(daoKey, serviceHub)
    }
  }

  override fun normalProposalsFor(daoKey: DaoKey): List<ProposalState<NormalProposal>> {
    return serviceHub.transaction {
      DataHelper.proposalStatesFor(daoKey, serviceHub)
    }
  }

  override fun modelDataProposalsFor(daoKey: DaoKey): List<ProposalState<ModelDataProposal>> {
    return serviceHub.transaction {
      DataHelper.proposalStatesFor(daoKey, serviceHub)
    }
  }

  override fun createNormalProposal(name: String, description: String, daoKey: DaoKey): Future<ProposalState<NormalProposal>> {
    return createProposal(NormalProposal(name, description), daoKey)
  }

  override fun createNewMemberProposal(daoName: String, sponsor: CordaX500Name): Future<ProposalState<MemberProposal>> {
    return sponsorCreateProposal(MemberProposal(me, daoName), daoName, sponsor)
  }

  override fun createRemoveMemberProposal(daoKey: DaoKey, member: CordaX500Name): Future<ProposalState<MemberProposal>> {
    val memberParty = findNode(member)
    return createProposal(MemberProposal(memberParty, daoKey.name, MemberProposal.Type.RemoveMember), daoKey)
  }

  override fun requestProposalConsistencyCheckFor(daoKey: DaoKey): Future<Set<ProposalKey>> {
    log.info("asking random dao member to add me to any proposals, for dao $daoKey, that they have which i do not belong to")

    val flowFuture = serviceHub.startFlow(RequestProposalConsistencyCheckFlow(daoKey)).returnValue
    return flowFuture.toVertxFuture()
  }

  override fun createIssuanceProposal(tokenType: TokenType.Descriptor, amount: String, daoKey: DaoKey): Future<ProposalState<IssuanceProposal>> {
    return createProposal(IssuanceProposal(tokenType, amount), daoKey)
  }

  override fun createModelDataProposal(name: String, newModelData: ModelData, daoKey: DaoKey): Future<ProposalState<ModelDataProposal>> {
    return createProposal(ModelDataProposal(name, newModelData), daoKey)
  }

  override fun listNodes(): List<CordaX500Name> {
    return serviceHub.networkMapCache.allNodes.map{ it.legalIdentities.first().name }
  }

  private fun findNotary(notary: CordaX500Name): Party {
    return serviceHub.networkMapCache.getNotary(notary) ?: throw RuntimeException("could not find notary $notary")
  }

  private fun findNode(node: CordaX500Name): Party {
    return serviceHub.networkMapCache.getPeerByLegalName(node) ?: throw RuntimeException("could not find node $node")
  }
}