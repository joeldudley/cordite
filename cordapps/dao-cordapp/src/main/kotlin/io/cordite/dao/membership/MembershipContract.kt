/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.membership

import io.cordite.dao.proposal.ProposalContract
import io.cordite.dao.proposal.ProposalState
import net.corda.core.contracts.*
import net.corda.core.identity.Party
import net.corda.core.transactions.LedgerTransaction
import net.corda.core.transactions.TransactionBuilder

val MEMBERSHIP_CONTRACT_ID = "io.cordite.dao.membership.MembershipContract"

open class MembershipContract : Contract {

  companion object {

    fun generateCreate(creator: Party, membershipKey: MembershipKey, notary: Party): Pair<MembershipState, TransactionBuilder> {
      val newState = MembershipState(setOf(creator), membershipKey)
      val contractAndState = StateAndContract(newState, MEMBERSHIP_CONTRACT_ID)
      val cmd = Command(Commands.CreateMembershipState(), listOf(creator.owningKey))

      val txBuilder = TransactionBuilder(notary = notary)
      txBuilder.withItems(contractAndState, cmd)

      return Pair(newState, txBuilder)
    }

    fun generateChangeMember(existingMembershipTxStateAndRef: StateAndRef<MembershipState>, acceptedProposalTxStateAndRef: StateAndRef<ProposalState<MemberProposal>>, notary: Party): Pair<MembershipState, TransactionBuilder> {
      val existingMembershipState = existingMembershipTxStateAndRef.state.data
      val acceptedProposal = acceptedProposalTxStateAndRef.state.data
      val changeMember = acceptedProposal.proposal.member
      val txBuilder = TransactionBuilder(notary = notary)

      val (outputMembershipState, membershipCommand) = when (acceptedProposal.proposal.type) {
        MemberProposal.Type.NewMember -> generateAddStateAndCommand(existingMembershipState, changeMember)
        MemberProposal.Type.RemoveMember -> generateRemoveStateAndCommand(existingMembershipState, changeMember)
      }

      val outputMembershipContractAndState = StateAndContract(outputMembershipState, MEMBERSHIP_CONTRACT_ID)
      val proposalCommand = Command(ProposalContract.Commands.ConsumeProposal(), acceptedProposal.participantSet().map { it.owningKey })

      txBuilder.withItems(acceptedProposalTxStateAndRef, proposalCommand, existingMembershipTxStateAndRef, outputMembershipContractAndState, membershipCommand)

      return Pair(outputMembershipState, txBuilder)
    }

    private fun generateAddStateAndCommand(existingMembershipState: MembershipState, changeMember: Party): Pair<MembershipState, Command<Commands.AddMember>> {
      if (existingMembershipState.members.contains(changeMember)) {
        throw IllegalArgumentException("cannot add member to membership state again")
      }
      val outputMembershipState = existingMembershipState.copyWith(changeMember)
      val membershipCommand = Command(Commands.AddMember(changeMember), outputMembershipState.members.map { it.owningKey })
      return Pair(outputMembershipState, membershipCommand)
    }

    private fun generateRemoveStateAndCommand(existingMembershipState: MembershipState, changeMember: Party): Pair<MembershipState, Command<Commands.RemoveMember>> {
      if (!existingMembershipState.members.contains(changeMember)) {
        throw IllegalArgumentException("cannot remove member that doesn't exist")
      }
      val outputMembershipState = existingMembershipState.copyWithout(changeMember)
      val membershipCommand = Command(Commands.RemoveMember(changeMember), outputMembershipState.members.map { it.owningKey })
      return Pair(outputMembershipState, membershipCommand)
    }

    fun generateReferToMembershipState(existingMembershipTxStateAndRef: StateAndRef<MembershipState>, txBuilder: TransactionBuilder) {
      val outputMembershipState = existingMembershipTxStateAndRef.state.data // do we need to create a dummy copy or can we get away with referring
      val outputMembershipContractAndState = StateAndContract(outputMembershipState, MEMBERSHIP_CONTRACT_ID)
      val cmd = Command(Commands.ReferToMembershipState(), outputMembershipState.members.map { it.owningKey })

      txBuilder.withItems(existingMembershipTxStateAndRef, outputMembershipContractAndState, cmd)
    }

  }

  override fun verify(tx: LedgerTransaction) {
    val command = tx.commands.requireSingleCommand<Commands>()
    val groups = tx.groupStates { it: MembershipState -> it.membershipKey }

    for ((inputs, outputs, _) in groups) {

      // do we want to include removed members later? i think yes because otherwise someone can unilaterally remove everyone else

      when (command.value) {
        is Commands.CreateMembershipState -> {
          requireThat {
            "no inputs should be consumed when creating a membership state" using (inputs.isEmpty())
            "there should be one output state" using (outputs.size == 1)
            "exactly members must be signers" using (outputs.first().members.map { it.owningKey } == command.signers)
          }

        }

        is Commands.AddMember -> {
          requireThat {
            "there should be one input state" using (inputs.size == 1)
            "there should be one output state" using (outputs.size == 1)

            val inputState = inputs.first()
            val outputState = outputs.first()
            val newMember = (command.value as Commands.AddMember).newMember

            "no members should be removed" using (outputState.members.containsAll(inputState.members))
            "newMember should be in output members" using (outputState.members.contains(newMember))
            "newMember should not be in input members" using (!inputState.members.contains(newMember))
            "exactly members must be signers" using (outputs.first().members.map { it.owningKey } == command.signers)
          }

        }

        is Commands.ReferToMembershipState -> {
          requireThat {
            "there should be one input state" using (inputs.size == 1)
            "there should be one output state" using (outputs.size == 1)

            val inputState = inputs.first()
            val outputState = outputs.first()

            "the input and output state should be the same" using (inputState == outputState)
          }
        }

        is Commands.RemoveMember -> {
          requireThat {
            "there should be one input state" using (inputs.size == 1)
            "there should be one output state" using (outputs.size == 1)

            val inputState = inputs.first()
            val outputState = outputs.first()
            val removeMember = (command.value as Commands.RemoveMember).removeMember

            "inputState should contain all of the output state members" using (inputState.members.containsAll(outputState.members))
            "outputState should have one fewer members" using (inputState.members.size == outputState.members.size + 1)
            "removeMember should be in input members" using (inputState.members.contains(removeMember))
            "removeMember should not be in output members" using (!outputState.members.contains(removeMember))
            "exactly members must be signers" using (outputs.first().members.map { it.owningKey } == command.signers)
          }
        }

        else -> throw IllegalArgumentException("unrecognised command: ${command.value}")
      }
    }
  }

  // Used to indicate the transaction's intent.
  interface Commands : CommandData {
    class CreateMembershipState : TypeOnlyCommandData(), Commands
    class AddMember(val newMember: Party) : Commands
    class RemoveMember(val removeMember: Party) : Commands
    class ReferToMembershipState : TypeOnlyCommandData(), Commands
  }

}