/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.proposal

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dao.data.DataHelper
import io.cordite.commons.utils.contextLogger
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.node.ServiceHub
import net.corda.core.serialization.CordaSerializable
import net.corda.core.utilities.unwrap

/*
we really only have four ways of handling this:

 1. Put _everything_ on the DaoState
    * horrific due to concurrency issues
 2. Include an unspent DaoState in every tx that creates a proposal or votes
    * this is logically equivalent to 1
    * no concurrency - but then maybe we don't care
    * on the plus side it's very deterministic and simple
 3. Same as above but don't spend it - just refer and check it's valid
    * think this uses oracles
    * not very deterministic - is it idempotent
    * not supported by corda and no plans to support
 4. Proposals just "have to have" Dao members as signers
    * if they don't, we don't care about them
    * if they do, we have some concurrency gains
    * at a cost of having to maintain the membership lists

The simplest and safest is 1 or 2, but there are concurrency issues.  The 4th is the other option.
Really easy, but quite a bit of work to do at membership change time.  It's safe work, though, as you
would do it in the same tx as making the membership change.

Going with 4/. fairly arbitrarily for the mo.
 */


@InitiatingFlow
@StartableByRPC
@StartableByService
class SponsorProposalFlow<T>(val sponsorAction: SponsorAction<T>, private val sponsor: Party) : FlowLogic<T>() {

  companion object {
    private val log = contextLogger()
  }

  @Suppress("UNCHECKED_CAST")
  @Suspendable
  override fun call(): T {
    log.info("asking $sponsor to sponsor: $sponsorAction")

    val proposerFlowSession = initiateFlow(sponsor)
    val proposalState = proposerFlowSession.sendAndReceive<Any>(sponsorAction)

    proposalState.unwrap {
      return it as T
    }
  }
}

@InitiatedBy(SponsorProposalFlow::class)
class SponsorProposalFlowResponder(private val creatorSession: FlowSession) : FlowLogic<Unit>() {

  companion object {
    private val log = contextLogger()
  }

  @Suspendable
  override fun call() {
    log.info("sponsor accepting the member, adding to dao and sending on proposals...")
    creatorSession.receive<SponsorAction<Any>>().unwrap { it ->
      creatorSession.send(it.execute(this, serviceHub))
    }
  }
}

@CordaSerializable
interface SponsorAction<T> {
  @Suspendable
  fun execute(flowLogic: FlowLogic<Unit>, serviceHub: ServiceHub): T
}

@CordaSerializable
class AcceptSponsorAction(val proposalKey: ProposalKey) : SponsorAction<ProposalLifecycle> {

  @Suspendable
  override fun execute(flowLogic: FlowLogic<Unit>, serviceHub: ServiceHub): ProposalLifecycle {
    return flowLogic.subFlow(AcceptProposalFlow(proposalKey))
  }

}

@CordaSerializable
class CreateSponsorAction<T : Proposal>(val proposal: T, val daoName: String) : SponsorAction<ProposalState<T>> {
  @Suspendable
  override fun execute(flowLogic: FlowLogic<Unit>, serviceHub: ServiceHub): ProposalState<T> {
    val daoKey = DataHelper.daoStateFor(daoName, serviceHub).daoKey
    return flowLogic.subFlow(CreateProposalFlow(proposal, daoKey))
  }
}