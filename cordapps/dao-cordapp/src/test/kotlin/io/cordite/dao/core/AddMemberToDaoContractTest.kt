/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.core

import io.cordite.dao.daoState
import io.cordite.dao.membership.MEMBERSHIP_CONTRACT_ID
import io.cordite.dao.membership.MembershipContract
import io.cordite.dao.membership.MembershipKey
import io.cordite.dao.membership.MembershipModelData
import io.cordite.dao.membershipState
import io.cordite.test.utils.*
import net.corda.testing.node.ledger
import org.junit.Test

class AddMemberToDaoContractTest {

  private val originalState = daoState()
  private val originalState1MinMember = originalState.copyWith(MembershipModelData(MembershipKey("wibble"), 1, false, false))

  @Test
  fun `happy path should verify`() {
    ledgerServices.ledger {
      transaction {
        input(DAO_CONTRACT_ID, originalState)
        output(DAO_CONTRACT_ID, originalState.copyWith(newMemberParty))
        command(listOf(proposerKey.public, newMemberKey.public), DaoContract.Commands.AddMember(newMemberParty))
        verifies()
      }
    }
  }

  @Test
  fun `should blow up if we do not update hasMinimumMembers`() {
    ledgerServices.ledger {
      transaction {
        input(DAO_CONTRACT_ID, originalState1MinMember)
        output(DAO_CONTRACT_ID, originalState1MinMember.copyWith(newMemberParty))
        command(listOf(proposerKey.public, newMemberKey.public), DaoContract.Commands.AddMember(newMemberParty))
        `fails with`("hasMinNumberOfMembers should be correct")
      }
    }
  }

  @Test
  fun `there should be one input state`() {
    ledgerServices.ledger {
      transaction {
        output(DAO_CONTRACT_ID, originalState)
        command(proposerKey.public, DaoContract.Commands.AddMember(newMemberParty))
        `fails with`("there should be one input state")
      }
    }
  }

  @Test
  fun `there should be one output state`() {
    ledgerServices.ledger {
      transaction {
        val daoState = daoState()
        input(DAO_CONTRACT_ID, originalState)
        output(DAO_CONTRACT_ID, daoState)
        output(DAO_CONTRACT_ID, daoState)
        command(proposerKey.public, DaoContract.Commands.AddMember(newMemberParty))
        `fails with`("there should be one output state")
      }
    }
  }

  @Test
  fun `the new member must be a signer`() {
    ledgerServices.ledger {
      transaction {
        input(DAO_CONTRACT_ID, originalState)
        output(DAO_CONTRACT_ID, originalState.copyWith(newMemberParty))
        command(proposerKey.public, DaoContract.Commands.AddMember(newMemberParty))
        `fails with`("exactly members must be signers")
      }
    }
  }

  @Test
  fun `the original member must be a signer`() {
    ledgerServices.ledger {
      transaction {
        input(DAO_CONTRACT_ID, originalState)
        output(DAO_CONTRACT_ID, originalState.copyWith(newMemberParty))
        command(newMemberKey.public, DaoContract.Commands.AddMember(newMemberParty))
        `fails with`("exactly members must be signers")
      }
    }
  }

  @Test
  fun `cannot remove original member`() {
    ledgerServices.ledger {
      transaction {
        input(DAO_CONTRACT_ID, originalState)
        output(DAO_CONTRACT_ID, originalState.copyWith(newMemberParty).copyWithout(proposerParty))
        command(newMemberKey.public, DaoContract.Commands.AddMember(newMemberParty))
        `fails with`("no members should be removed")
      }
    }
  }

  @Test
  fun `newMember should be in output members`() {
    ledgerServices.ledger {
      transaction {
        input(DAO_CONTRACT_ID, originalState)
        output(DAO_CONTRACT_ID, originalState)
        command(newMemberKey.public, DaoContract.Commands.AddMember(newMemberParty))
        `fails with`("newMember should be in output members")
      }
    }
  }

  @Test
  fun `newMember should not be in input members`() {
    ledgerServices.ledger {
      transaction {
        input(DAO_CONTRACT_ID, originalState)
        output(DAO_CONTRACT_ID, originalState)
        command(newMemberKey.public, DaoContract.Commands.AddMember(proposerParty))
        `fails with`("newMember should not be in input members")
      }
    }
  }
}