/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.proposal


import io.cordite.dao.*
import io.cordite.dao.proposal.PROPOSAL_CONTRACT_ID
import io.cordite.dao.proposal.ProposalContract
import io.cordite.test.utils.*
import net.corda.testing.node.ledger
import org.junit.Test

// NB we cannot really check that all the DaoState members are participants in the current scheme, so members
// must check this in the responder core - discussion in CreateProposalFlow
class VoteForProposalContractTest {

  private val initialProposal = proposalState().copyWithNewMember(voterParty)
  private val outputProposalState = initialProposal.copyWithNewSupporter(voterParty)

  @Test
  fun `happy path should verify`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        command(listOf(proposerKey.public, voterKey.public), ProposalContract.Commands.VoteForProposal(voterParty))
        verifies()
      }
    }
  }

  @Test
  fun `there should be one input state`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        command(listOf(proposerKey.public, voterKey.public), ProposalContract.Commands.VoteForProposal(voterParty))
        `fails with`("There should be one input proposal")
      }
    }
  }


  @Test
  fun `there should be one output state`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        command(listOf(proposerKey.public, voterKey.public), ProposalContract.Commands.VoteForProposal(voterParty))
        `fails with`("There should be one output proposal")
      }
    }
  }

  @Test
  fun `proposers should be the same`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, proposalCopy(initialProposal, proposer = daoParty))
        command(listOf(proposerKey.public, voterKey.public), ProposalContract.Commands.VoteForProposal(voterParty))
        `fails with`("proposers should be the same")
      }
    }
  }

  @Test
  fun `no members should be removed`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, proposalCopy(initialProposal, members = setOf(proposerParty)))
        command(listOf(proposerKey.public, voterKey.public), ProposalContract.Commands.VoteForProposal(voterParty))
        `fails with`("no members should be removed")
      }
    }
  }

  @Test
  fun `supporters should be old set plus new supporter`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, initialProposal)
        command(listOf(proposerKey.public, voterKey.public), ProposalContract.Commands.VoteForProposal(voterParty))
        `fails with`("supporters should be old set plus new supporter")
      }
    }
  }

  @Test
  fun `proposer must be signer`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        command(voterKey.public, ProposalContract.Commands.VoteForProposal(voterParty))
        `fails with`("proposer must be signer")
      }
    }
  }

  @Test
  fun `supporters must be members`() {
    ledgerServices.ledger {
      transaction {
        val inputProp = proposalState()
        input(PROPOSAL_CONTRACT_ID, inputProp)
        output(PROPOSAL_CONTRACT_ID, inputProp.copyWithNewSupporter(voterParty))
        command(listOf(proposerKey.public, voterKey.public), ProposalContract.Commands.VoteForProposal(voterParty))
        `fails with`("supporters must be members")
      }
    }
  }

  @Test
  fun `all members must be signers`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        command(proposerKey.public, ProposalContract.Commands.VoteForProposal(voterParty))
        `fails with`("members must be signers")
      }
    }
  }

}