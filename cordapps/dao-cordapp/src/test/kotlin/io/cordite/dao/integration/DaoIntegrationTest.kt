/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.integration

import io.bluebank.braid.client.BraidProxyClient
import io.cordite.commons.utils.contextLogger
import io.cordite.dao.DaoApi
import io.cordite.dao.assertDaoStateContainsMembers
import io.cordite.dao.coop.Address
import io.cordite.dao.coop.CoopModelData
import io.cordite.dao.core.DaoState
import io.cordite.dao.core.ModelData
import io.cordite.dao.core.SampleModelData
import io.cordite.dao.daoCordappPackages
import io.cordite.dao.economics.DAO_TAG
import io.cordite.dao.economics.EconomicsModelData
import io.cordite.dao.membership.MemberProposal
import io.cordite.dao.proposal.NormalProposal
import io.cordite.dao.proposal.ProposalLifecycle
import io.cordite.dao.proposal.ProposalState
import io.cordite.dgl.corda.LedgerApi
import io.cordite.dgl.corda.token.TokenType
import io.cordite.test.utils.*
import io.vertx.ext.unit.TestContext
import io.vertx.ext.unit.junit.VertxUnitRunner
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.StartedMockNode
import org.junit.AfterClass
import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.reflect.Member


class TestNode(val node: StartedMockNode, braidPortHelper: BraidPortHelper) {

  private val daoBraidClient: BraidProxyClient
  private val dglBraidClient: BraidProxyClient

  val party: Party = node.info.legalIdentities.first()
  val daoApi: DaoApi
  val dglApi: LedgerApi

  init {
    daoBraidClient = BraidClientHelper.braidClient(braidPortHelper.portForParty(party), "daoservice", "localhost")
    daoApi = daoBraidClient.bind(DaoApi::class.java)
    dglBraidClient = BraidClientHelper.braidClient(braidPortHelper.portForParty(party), "ledger", "localhost")
    dglApi = dglBraidClient.bind(LedgerApi::class.java)
  }

  fun shutdown() {
    daoBraidClient.close()
    dglBraidClient.close()
  }

}

@RunWith(VertxUnitRunner::class)
class DaoIntegrationTest {

  companion object {
    private val log = contextLogger()
    private const val casaDaoBase = "casaDao"
    private const val proposalBase = "proposal"

    private lateinit var network: MockNetwork

    private val braidPortHelper = BraidPortHelper()

    private lateinit var proposer: TestNode
    private lateinit var newMember: TestNode
    private lateinit var anotherMember: TestNode
    private lateinit var notaryName: CordaX500Name

    private var salt = 0

    @BeforeClass
    @JvmStatic
    fun setup() {

      braidPortHelper.setSystemPropertiesFor(proposerName, newMemberName, anotherMemberName)
      network = MockNetwork(cordappPackages = daoCordappPackages())

      proposer = TestNode(network.createPartyNode(proposerName), braidPortHelper)
      newMember = TestNode(network.createPartyNode(newMemberName), braidPortHelper)
      anotherMember = TestNode(network.createPartyNode(anotherMemberName), braidPortHelper)

      network.runNetwork()

      notaryName = network.defaultNotaryIdentity.name
    }

    @AfterClass
    @JvmStatic
    fun tearDown() {
      proposer.shutdown()
      newMember.shutdown()
      anotherMember.shutdown()
      try {
        network.stopNodes()
      } catch (e: NullPointerException) {
        log.info(e.message)
      }
    }

  }

  private val saltedDaoName = casaDaoBase + salt++
  private val saltedProposalName = proposalBase + salt++

  @Test
  fun `should be able to get whitelist`() {
    val config = proposer.daoApi.nodeConfig()
    Assert.assertTrue("config should contain notary info", config.contains("NotaryInfo(identity=O=Notary Service, L=Zurich, C=CH, validating=true)"))
    Assert.assertTrue("config should contain node info", config.contains("NodeInfo(addresses=[mock.node:1000], legalIdentitiesAndCerts=[O=Proposer, L=London, C=GB], platformVersion=1"))
  }

  @Test
  fun `should be able to get node list for chris for the mo - move this`() {
    Assert.assertEquals("incorrect number of nodes", 4, proposer.daoApi.listNodes().size)
  }

  @Test
  fun `default dao should be strict and have min members of 3`() {
    val daoStateMembershipModelData = run(network) { proposer.daoApi.createDao(saltedDaoName, notaryName) }.membershipModelData()
    Assert.assertEquals("default min members should have been 3", 3, daoStateMembershipModelData.minimumMemberCount)
    Assert.assertEquals("default strict mode should be true", true, daoStateMembershipModelData.strictMode)
  }

  @Test
  fun `should be able to create a dao`() {
    createDaoWithName(saltedDaoName, notaryName)
  }

  @Test
  fun `should be able to specify data needed for list daos table`() {
    val coopModelData = CoopModelData("these are my objects", Address.from("some","address","lines"))
    val mdSet: Array<ModelData> = arrayOf(coopModelData)
    val daoState = run(network) { proposer.daoApi.createDao(saltedDaoName, mdSet, notaryName) }
    Assert.assertEquals("coop model datas should be the same", coopModelData, daoState.get(CoopModelData::class))
  }

  @Test
  fun `should be able to create another dao in the same node`() {
    // because the salt changes for each test run this is actually a different dao..
    createDaoWithName(saltedDaoName, notaryName)
  }

  @Test
  fun `should be able to get list of daos`() {
    val initialDaoCount = proposer.daoApi.listDaos().size
    createDaoWithName(saltedDaoName, notaryName)
    val newDaoCount = proposer.daoApi.listDaos().size
    Assert.assertEquals("incorrect number of daos", initialDaoCount + 1, newDaoCount)
  }

  @Test
  fun `should be able to add a new member`() {
    val dao = createDaoWithName(saltedDaoName, notaryName)
    val proposal = run(network) { newMember.daoApi.createNewMemberProposal(saltedDaoName, proposer.party.name) }
    Assert.assertEquals("should be 1 supporters", 1, proposal.supporters.size)
    Assert.assertTrue("member should be a supporter", proposal.supporters.contains(newMember.party))

    // accept member proposal
    val acceptedProposalLifecycle = run(network) { newMember.daoApi.sponsorAcceptProposal(proposal.proposal.key(), dao.daoKey, proposer.party.name) }
    Assert.assertEquals("proposal should be accepted", ProposalLifecycle.ACCEPTED, acceptedProposalLifecycle)

    assertDaoStateContainsMembers(getDaoWithRetry(newMember), proposer.party, newMember.party)
  }

  @Test
  fun `should be able to add two members`() {
    createDaoWithName(saltedDaoName, notaryName)
    addMemberToDao(newMember, proposer, saltedDaoName)
    addMemberToDao(anotherMember, proposer, saltedDaoName, newMember)
  }

  @Test
  fun `should be able to create, vote for and accept and list new proposal`() {
    val originalProposalCount = proposer.daoApi.listProposalKeys().size
    val daoState = createDaoWithName(saltedDaoName, notaryName)
    addMemberToDao(newMember, proposer, saltedDaoName)
    createAndAcceptProposalWithName(saltedProposalName, proposer, daoState, newMember)

    val keys = proposer.daoApi.listProposalKeys()
    Assert.assertEquals("there should be only one proposal", originalProposalCount + 1, keys.size)
    val proposal = proposer.daoApi.proposalFor(keys.first())
    Assert.assertEquals("proposal should have correct key", keys.first(), proposal.proposal.key())
  }

  @Test
  fun `new member should receive proposals`() {
    val daoState = createDaoWithName(saltedDaoName, notaryName)
    createProposal(saltedProposalName, proposer, daoState)
    addMemberToDao(newMember, proposer, saltedDaoName)
    val proposals = newMember.daoApi.normalProposalsFor(daoState.daoKey)
    Assert.assertEquals("new member should now have the proposal", 1, proposals.size)
    Assert.assertEquals("should have correct proposal", saltedProposalName, proposals.first().name)
  }

  @Test
  fun `removing member from dao should remove them from proposals`() {
    val daoState = createDaoWithName(saltedDaoName, notaryName)
    addMemberToDao(newMember, proposer, saltedDaoName)
    addMemberToDao(anotherMember, proposer, saltedDaoName, newMember)
    createProposal(saltedProposalName, proposer, daoState)
    val proposals = newMember.daoApi.normalProposalsFor(daoState.daoKey)
    Assert.assertEquals("new member should now have the proposal", 1, proposals.size)
    Assert.assertEquals("there should be 3 members", 3, proposals.first().members.size)

    val removeProposal = run(network) { proposer.daoApi.createRemoveMemberProposal(daoState.daoKey, newMember.party.name) }

    Assert.assertEquals("should be 1 supporters", 1, removeProposal.supporters.size)
    Assert.assertTrue("proposer should be a supporter", removeProposal.supporters.contains(proposer.party))

    // other member approves
    run(network) { anotherMember.daoApi.voteForProposal(removeProposal.proposal.proposalKey) }

    val acceptedProposalLifecycle = run(network) { proposer.daoApi.acceptProposal(removeProposal.proposal.key()) }
    Assert.assertEquals("proposal should be accepted", ProposalLifecycle.ACCEPTED, acceptedProposalLifecycle)

    assertDaoStateContainsMembers(getDaoWithRetry(proposer), proposer.party, anotherMember.party)

    val currentProposals = proposer.daoApi.normalProposalsFor(daoState.daoKey)
    Assert.assertEquals("there should still be 1 proposal", 1, currentProposals.size)
    Assert.assertEquals("there should now be 2 members", 2, currentProposals.first().members.size)
  }

  @Test
  fun `removing member from dao should remove them from proposal supporters`() {
    val daoState = createDaoWithName(saltedDaoName, notaryName)
    addMemberToDao(newMember, proposer, saltedDaoName)
    addMemberToDao(anotherMember, proposer, saltedDaoName, newMember)
    val proposal = createProposal(saltedProposalName, proposer, daoState)

    // support the proposal
    forTheLoveOfGodIgnoreThisBit()
    run(network) { newMember.daoApi.voteForProposal(proposal.proposal.proposalKey) }

    val proposals = newMember.daoApi.normalProposalsFor(daoState.daoKey)
    Assert.assertEquals("new member should now have the proposal", 1, proposals.size)
    Assert.assertEquals("there should be 3 members", 3, proposals.first().members.size)
    Assert.assertTrue("new member should be supporter", proposals.first().supporters.contains(newMember.party))

    val removeProposal = run(network) { proposer.daoApi.createRemoveMemberProposal(daoState.daoKey, newMember.party.name) }

    Assert.assertEquals("should be 1 supporters", 1, removeProposal.supporters.size)
    Assert.assertTrue("proposer should be a supporter", removeProposal.supporters.contains(proposer.party))

    // other member approves
    run(network) { anotherMember.daoApi.voteForProposal(removeProposal.proposal.proposalKey) }

    val acceptedProposalLifecycle = run(network) { proposer.daoApi.acceptProposal(removeProposal.proposal.key()) }
    Assert.assertEquals("proposal should be accepted", ProposalLifecycle.ACCEPTED, acceptedProposalLifecycle)

    assertDaoStateContainsMembers(getDaoWithRetry(proposer), proposer.party, anotherMember.party)

    val currentProposals = proposer.daoApi.normalProposalsFor(daoState.daoKey)
    Assert.assertEquals("there should still be 1 proposal", 1, currentProposals.size)
    Assert.assertEquals("there should now be 2 members", 2, currentProposals.first().members.size)
    Assert.assertFalse("new member should not now be a supporter", currentProposals.first().supporters.contains(newMemberParty))
  }

  @Test
  fun `should be able to create a metering model data for a dao`() {
    val daoState = createDaoWithName(saltedDaoName, notaryName)
    addMemberToDao(newMember, proposer, saltedDaoName)

    Assert.assertFalse(daoState.containsModelData(SampleModelData::class))

    val newModelData = SampleModelData("initial")
    val proposalState = run(network) { proposer.daoApi.createModelDataProposal("fiddle with metering model data", newModelData, daoState.daoKey) }

    run(network) { newMember.daoApi.voteForProposal(proposalState.proposal.proposalKey) }

    val proposals = proposer.daoApi.modelDataProposalsFor(daoState.daoKey)

    Assert.assertEquals("there should only be one proposal", 1, proposals.size)
    Assert.assertEquals("the proposal should have 2 supporters", 2, proposals.first().supporters.size)
    Assert.assertTrue("the proposal should have all supporters", proposals.first().supporters.containsAll(setOf(proposer.party, newMember.party)))

    val acceptedProposalLifecycle = run(network) { proposer.daoApi.acceptProposal(proposalState.proposal.proposalKey) }
    Assert.assertEquals("proposal should have been accepted", ProposalLifecycle.ACCEPTED, acceptedProposalLifecycle)
    val newDaoState = proposer.daoApi.daoFor(daoState.daoKey)
    Assert.assertEquals("dao should contain metering model data", newModelData, newDaoState.get(newModelData::class))
  }

  // should probably be a number of tests in another test class once we've brought up the test env once
  @Test
  fun `should be able to issue plutus uber test`() {
    val daoState = createDaoWithName(saltedDaoName, notaryName)
    addMemberToDao(newMember, proposer, saltedDaoName)

    Assert.assertFalse(daoState.containsModelData(EconomicsModelData::class))

    val plutusModelData = EconomicsModelData(listOf(TokenType.Descriptor("XPLT", 2, proposerName)))

    val proposalState = run(network) { proposer.daoApi.createModelDataProposal("propose plutus", plutusModelData, daoState.daoKey) }

    run(network) { newMember.daoApi.voteForProposal(proposalState.proposal.proposalKey) }

    val acceptedProposalLifecycle = run(network) { proposer.daoApi.acceptProposal(proposalState.proposal.proposalKey) }
    Assert.assertEquals("proposal should have been accepted", ProposalLifecycle.ACCEPTED, acceptedProposalLifecycle)

    // check proposer has the token type
    val tokenTypes = run(network) { proposer.dglApi.listTokenTypes() }
    val xpltTokenType = tokenTypes.first { it.descriptor.symbol == "XPLT" }
    Assert.assertEquals("plutus token issuer should be proposer", proposer.party, xpltTokenType.issuer)

    val accountId = saltedDaoName

    listOf(proposer, newMember).forEach {
      val account = run(network) { it.dglApi.getAccount(accountId) }
      Assert.assertTrue("${it.party.name} account should have dao tag", account.tags.contains(DAO_TAG))
    }

    // add another member and check the member has the account
    addMemberToDao(anotherMember, proposer, saltedDaoName, newMember)
    val account = run(network) { anotherMember.dglApi.getAccount(accountId) }
    Assert.assertTrue("account should have dao tag", account.tags.contains(DAO_TAG))

    val plutusProposal = run(network) { proposer.daoApi.createIssuanceProposal(xpltTokenType.descriptor, "300", daoState.daoKey) }
    run(network) { newMember.daoApi.voteForProposal(plutusProposal.proposal.proposalKey) }
    val acceptedPlutusProposalLifecycle = run(network) { proposer.daoApi.acceptProposal(plutusProposal.proposal.proposalKey) }
    Assert.assertEquals("proposal should have been accepted", ProposalLifecycle.ACCEPTED, acceptedPlutusProposalLifecycle)

    listOf(proposer, newMember, anotherMember).forEach {
      val balance = run(network) { it.dglApi.balanceForAccount(accountId) }
      val xpltBalance = balance.filter { it.token == xpltTokenType.descriptor }
      Assert.assertEquals("balance should be 10000", 10000L, xpltBalance.first().quantity )
    }

  }

  @Test
  fun `should be able to receive dao updates using observable`(context: TestContext) {
    val daoState = createDaoWithName(saltedDaoName, notaryName)
    val async = context.async()

    // listen for expected update from...
    proposer.daoApi.listenForDaoUpdates().subscribe {
      val memberCount = it.members.size
      log.info("dao returned with member count: $memberCount")
      if (it.daoKey == daoState.daoKey && memberCount > 1) { // we get the first one back - not sure yet whether this is timing or by design.
        Assert.assertEquals("should be two members now", 2, memberCount)
        async.complete()
      }
    }

    // ...from adding a new member
    addMemberToDao(newMember, proposer, daoState.name)
  }

  @Test
  fun`should be able to receive proposal updates using observable`(context: TestContext) {
    val daoState = createDaoWithName(saltedDaoName, notaryName)
    val async = context.async()

    proposer.daoApi.listenForProposalUpdates().subscribe {
      log.info("proposal returned with key: ${it.proposal.key()}")
      if (it.daoKey == daoState.daoKey && it.proposal is MemberProposal) {
        val mp = it.proposal as MemberProposal
        Assert.assertEquals("should be new member type", MemberProposal.Type.NewMember, mp.type)
        async.complete()
      }
    }

    run(network) { newMember.daoApi.createNewMemberProposal(daoState.name, proposer.party.name) }
  }

  private fun getDaoWithRetry(testNode: TestNode): List<DaoState> {
    (1..5).forEach {
      log.info("trying to get dao list")
      val daos = testNode.daoApi.daoInfo(saltedDaoName)
      if (daos.isNotEmpty()) {
        log.info("phew - daos returned")
        return daos
      }
      log.info("dao state not arrived yet - snoozing")
      Thread.sleep(100)
    }
    throw RuntimeException("Unable to get daos from node - are you sure this is a race condition?")
  }

  private fun createDaoWithName(daoName: String, notaryName: CordaX500Name): DaoState {
    Assert.assertEquals("there should be no casa dao at the beginning", 0, proposer.daoApi.daoInfo(daoName).size)
    val daoState = run(network) { proposer.daoApi.createDao(daoName, 1, false, notaryName) }
    assertDaoStateContainsMembers(getDaoWithRetry(proposer), proposer.party)
    return daoState
  }

  private fun addMemberToDao(newMemberNode: TestNode, proposerNode: TestNode, daoName: String, vararg extraSigners: TestNode) {
    val proposal = run(network) { newMemberNode.daoApi.createNewMemberProposal(daoName, proposerNode.party.name) }

    Assert.assertEquals("should be 1 supporters", 1, proposal.supporters.size)
    Assert.assertTrue("member should be a supporter", proposal.supporters.contains(newMemberNode.party))

    var numberOfSupporters = 1
    extraSigners.forEach {
      run(network) { it.daoApi.voteForProposal(proposal.proposal.proposalKey) }
      val postVote = it.daoApi.proposalFor(proposal.proposal.proposalKey)
      Assert.assertEquals("there should be three supporters", ++numberOfSupporters, postVote.supporters.size)
    }

    // accept member proposal
    val acceptedProposalLifecycle = run(network) { newMember.daoApi.sponsorAcceptProposal(proposal.proposal.key(), proposal.daoKey, proposerNode.party.name) }
    Assert.assertEquals("proposal should be accepted", ProposalLifecycle.ACCEPTED, acceptedProposalLifecycle)

    assertDaoStateContainsMembers(getDaoWithRetry(newMemberNode), proposerNode.party, newMemberNode.party, *extraSigners.map { it.party }.toTypedArray())
  }

  private fun createAndAcceptProposalWithName(proposalName: String, proposalProposer: TestNode, daoState: DaoState, vararg supporters: TestNode) {
    val proposal = createProposal(proposalName, proposalProposer, daoState)

    supporters.forEach {
      run(network) { it.daoApi.voteForProposal(proposal.proposal.proposalKey) }
    }

    val proposals = proposalProposer.daoApi.normalProposalsFor(daoState.daoKey)

    Assert.assertEquals("there should only be one proposal", 1, proposals.size)
    Assert.assertEquals("the proposal should have ${supporters.size + 1} supporters", supporters.size + 1, proposals.first().supporters.size)
    Assert.assertTrue("the proposal should have all supporters", proposals.first().supporters.containsAll(setOf(proposalProposer.party, *supporters.map { it.party }.toTypedArray())))

    val acceptedProposalLifecycle = run(network) { proposalProposer.daoApi.acceptProposal(proposal.proposal.proposalKey) }
    Assert.assertEquals("proposal should have been accepted", ProposalLifecycle.ACCEPTED, acceptedProposalLifecycle)
  }

  private fun createProposal(proposalName: String, proposalProposer: TestNode, daoState: DaoState): ProposalState<NormalProposal> {
    val proposal = run(network) { proposalProposer.daoApi.createNormalProposal(proposalName, "some description", daoState.daoKey) }

    val origProposals = proposalProposer.daoApi.normalProposalsFor(daoState.daoKey)
    Assert.assertEquals("there should only be one proposal", 1, origProposals.size)
    Assert.assertEquals("the proposal should have one supporter", 1, origProposals.first().supporters.size)
    Assert.assertTrue("proposer should be supporter", origProposals.first().supporters.containsAll(setOf(proposalProposer.party)))
    Assert.assertEquals("keys should be the same", proposal.proposal.proposalKey, origProposals.first().proposal.proposalKey)
    return proposal
  }
}
