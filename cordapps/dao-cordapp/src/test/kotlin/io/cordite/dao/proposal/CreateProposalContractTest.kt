/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.proposal

import io.cordite.dao.*
import io.cordite.dao.proposal.PROPOSAL_CONTRACT_ID
import io.cordite.dao.proposal.ProposalContract
import io.cordite.test.utils.*
import net.corda.testing.node.ledger
import org.junit.Test


// NB we cannot really check that all the DaoState members are participants in the current scheme, so members
// must check this in the responder core - discussion in CreateProposalFlow
class CreateProposalContractTest {

  @Test
  fun `happy path should verify`() {
    ledgerServices.ledger {
      transaction {
        output(PROPOSAL_CONTRACT_ID, proposalState())
        command(proposerKey.public, ProposalContract.Commands.CreateProposal())
        verifies()
      }
    }
  }

  @Test
  fun `propose should not consume state`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, proposalState())
        command(proposerKey.public, ProposalContract.Commands.CreateProposal())
        `fails with`("No inputs should be consumed when creating a proposal")
      }
    }
  }

  @Test
  fun `there should be one output state`() {
    ledgerServices.ledger {
      transaction {
        val proposalState = proposalState()
        output(PROPOSAL_CONTRACT_ID, proposalState)
        output(PROPOSAL_CONTRACT_ID, proposalState)
        command(proposerKey.public, ProposalContract.Commands.CreateProposal())
        `fails with`("There should be one output proposal")
      }
    }
  }

  @Test
  fun `proposer must be only initial supporter`() {
    ledgerServices.ledger {
      transaction {
        output(PROPOSAL_CONTRACT_ID, proposalState(supporters = setOf(daoParty)))
        command(daoKey.public, ProposalContract.Commands.CreateProposal())
        `fails with`("proposer must be only initial supporter")
      }
    }
  }

  @Test
  fun `proposer must be signer`() {
    ledgerServices.ledger {
      transaction {
        output(PROPOSAL_CONTRACT_ID, proposalState())
        command(daoKey.public, ProposalContract.Commands.CreateProposal())
        `fails with`("proposer must be signer")
      }
    }
  }

  @Test
  fun `all members must be signers`() {
    ledgerServices.ledger {
      transaction {
        output(PROPOSAL_CONTRACT_ID, proposalState(members = setOf(proposerParty, daoParty)))
        command(proposerKey.public, ProposalContract.Commands.CreateProposal())
        `fails with`("members must be signers")
      }
    }
  }

}