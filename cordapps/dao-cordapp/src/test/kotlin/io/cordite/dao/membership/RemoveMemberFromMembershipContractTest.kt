/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.membership

import io.cordite.dao.*
import io.cordite.dao.membership.MEMBERSHIP_CONTRACT_ID
import io.cordite.dao.membership.MembershipContract
import io.cordite.test.utils.*
import net.corda.testing.node.ledger
import org.junit.Test

class RemoveMemberFromMembershipContractTest {

  private val originalState = membershipState().copyWith(newMemberParty)
  private val outputState = originalState.copyWithout(newMemberParty)

  @Test
  fun `happy path should verify`() {
    ledgerServices.ledger {
      transaction {
        input(MEMBERSHIP_CONTRACT_ID, originalState)
        output(MEMBERSHIP_CONTRACT_ID, outputState)
        command(proposerKey.public, MembershipContract.Commands.RemoveMember(newMemberParty))
        verifies()
      }
    }
  }


  @Test
  fun `there should be one input state`() {
    ledgerServices.ledger {
      transaction {
        output(MEMBERSHIP_CONTRACT_ID, originalState)
        command(proposerKey.public, MembershipContract.Commands.RemoveMember(newMemberParty))
        `fails with`("there should be one input state")
      }
    }
  }

  @Test
  fun `there should be one output state`() {
    ledgerServices.ledger {
      transaction {
        val daoState = daoState()
        input(MEMBERSHIP_CONTRACT_ID, originalState)
        output(MEMBERSHIP_CONTRACT_ID, daoState)
        output(MEMBERSHIP_CONTRACT_ID, daoState)
        command(proposerKey.public, MembershipContract.Commands.RemoveMember(newMemberParty))
        `fails with`("there should be one output state")
      }
    }
  }

  @Test
  fun `inputState should contain all of the output state members`() {
    ledgerServices.ledger {
      transaction {
        input(MEMBERSHIP_CONTRACT_ID, originalState)
        output(MEMBERSHIP_CONTRACT_ID, outputState.copyWith(anotherMemberParty))
        command(proposerKey.public, MembershipContract.Commands.RemoveMember(newMemberParty))
        `fails with`("inputState should contain all of the output state members")
      }
    }
  }

  @Test
  fun `outputState should have one fewer members`() {
    ledgerServices.ledger {
      transaction {
        input(MEMBERSHIP_CONTRACT_ID, originalState)
        output(MEMBERSHIP_CONTRACT_ID, originalState)
        command(newMemberKey.public, MembershipContract.Commands.RemoveMember(newMemberParty))
        `fails with`("outputState should have one fewer members")
      }
    }
  }

  @Test
  fun `removeMember should be in input members`() {
    ledgerServices.ledger {
      transaction {
        input(MEMBERSHIP_CONTRACT_ID, originalState)
        output(MEMBERSHIP_CONTRACT_ID, outputState)
        command(newMemberKey.public, MembershipContract.Commands.RemoveMember(anotherMemberParty))
        `fails with`("removeMember should be in input members")
      }
    }
  }

  @Test
  fun `removeMember should not be in output members`() {
    ledgerServices.ledger {
      transaction {
        input(MEMBERSHIP_CONTRACT_ID, originalState)
        output(MEMBERSHIP_CONTRACT_ID, outputState)
        command(newMemberKey.public, MembershipContract.Commands.RemoveMember(proposerParty))
        `fails with`("removeMember should not be in output members")
      }
    }
  }

  @Test
  fun `exactly members must be signers`() {
    ledgerServices.ledger {
      transaction {
        input(MEMBERSHIP_CONTRACT_ID, originalState)
        output(MEMBERSHIP_CONTRACT_ID, outputState)
        command(newMemberKey.public, MembershipContract.Commands.RemoveMember(newMemberParty))
        `fails with`("exactly members must be signers")
      }
    }
  }
}