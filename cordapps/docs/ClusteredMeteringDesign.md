<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
# Clustered Metering

## Overview

Metering on clustered notaries presents a few technical challenges. At the moment, Corda provides a RAFT cluster and a Smart BFT Cluster. At this point in time it is likely that the implementation of the BFT cluster will change. In this document we aim to produce an approach that will work for all types of clusters, be they completely replicated (RAFT) or replicated with partial consensus (BFT).

## Metering Overview

Currently metering works on a Single Validating or non Validating Notary.
The initial aim of the metering code was not to change any of the core corda code and so it was built in a completely separate repo (cordite). Metering can be 'installed' on a notary by adding the cordite jar to the plugins folder and adding a small json config file.
Metering also has a dispersal service which distributes collected 'fees' to a group of parties.
At the moment the dispersal service runs on one of the 'DAO' nodes.

The following descriptions of the metering and dispersal service are one way of doing this function and should be seen as a 'model' which can be adapted to lots of different models, such as:
1. Pay as you go - i.e. every transaction is logged and then invoiced.
2. PrePaid metering - i.e. a number of transactions are paid for up front for a discount and then a higher price then these are used up (think mobile phone data contracts).
3. Freemium - i.e. a free transactions up to 1000 then you pay for them.
4. Delay Repay - i.e. use now and pay in 90 days - classic b2b invoice model.

### What the metering service currently does

The metering service reads a small config file, if the legal name in the config file matches the nodes legal name,
the service will start to meter or exit.
When metering starts, the rest of the metering 'rules' are loaded from a DAO State.
The metering service runs on a configurable poll of 2 seconds. It queries a left join between the notary commit log and the metering invoice table.
If it finds any transactions with a 'null' for the metering invoice table, it creates a metering invoice and sends it via a flow to the party that created the transaction. 
After this point the metering service has done it's job.
The 'client' can then pay or dispute a metering invoice via the cordite api and a standard set of collectSigs/Finality flows.
At the moment, the metering node will receive 'disputed' invoice and be able to re-issue them with restricted changes.
As the metering service is running on a notary, we don't trust it to notarise it's own flows/states, so we use another notary called a 'Guardian' to notaries these transactions.
The guardian also loads the Cordite Jars so that is can receive and spend funds, but does not run the Metering Service.

### What the fee dispersal service currently does.

When clients pay their metering invoice the 'PAID' metering invoice and a payment will go to the fee dispersal service and the fee (in tokens) is stored in a holding account. Note: the token can be any type of Cordite Token which is based on fungible state.
The fee dispersal service is owned by a DAO member.
The fee dispersal service runs on a 2 second poll and checks for PAID metering invoices.
When it finds a paid invoice, it checks the metering rules and then allocates the payment to the parties who 'helped' with the transaction.
The example rules split the payment into three. 50% goes to the metering notary, 10% goes to the guardian notary and 40% does to the DAO fund - all this in configurable.

A more detailed description of the whats, whys and hows of metering can be found [here](https://medium.com/corda/cordites-metering-notaries-a-deeper-dive-8803d6af269a)


## Key challenges for clustered metering

As the metering service currently runs on a single node, it needs to be refactored to run in a clustered environment.
The key challenges are:

1. Ensuring only one of the Notaries in the cluster issues the metering invoice.
2. Ensuring that the metering service is not affected by notaries leaving or joining the cluster.
3. Ensuring that metering invoices raised by malicious notaries are rejected by the cluster (note: the client will reject any invoices where it kind find the transaction id in its vault).
4. Ensuring that dispute resolution can still be managed (this is currently done on the metering notary).
5. Security of notary services is not compromised by metering.

The following sections give a brief overview of how the current clusters work and the proposed approach to enable metering to function on these clusters.


## Brief description of the RAFT Notary.

The RAFT notary functionality is primarily handled by the RaftUniquenessProvider class.
This class consumes the Raft functionality provided by the atomix library (https://atomix.io/)
The uniqueness provider acts as a member of the cluster as well as being a client of the cluster.
Notarised transactions are stored in a RaftState class which is stored in a DistributedImmutableMap. This map is replicated and persisted to all nodes.
At the moment the RaftState does not hold the Identity of the creator of the transaction which we need in order to create a metering invoice.
The ‘commit’ function on the uniqueness provider commits new transaction states to the DistributedImmutableMap via the CopyCat Client. The CopyCat client distributes the transaction across the cluster and checks that the transaction is unique.
As with all specialised notaries, the Raft notary provides the ‘receiver’ end of the notary flow.
One outstanding question is how the customer ‘node’ decides which member of the notary cluster to send the initial transaction to as it uses a shared legal entity - the code does not seem to reveal this easily (Note to self : debug this when you get a notary cluster set up)

## Brief Description of the BFT Smart Notary
Y
TBA

## Approach 1 for metering RAFT notary - 'Leader/Meterer'

The cordite metering service runs on all nodes in the cluster

The Raft Notary is amended to provide a callback hook which the the metering service can listen to for new transactions.
If it is the leader, it takes the new transaction and puts this on a ‘metering’ distributed map with the metering state as UN_METERED,

As it is a unique map, only non duplicate metering invoices will be added.

We create a RaftMeterer derived from AbstractMeterer that periodically checks the Metering Map for UN_METERED transactions.
If we are the RAFT leader, then a metering invoice is created and sent to the original client - this will only go into the vault of the Leader (see potential problems below)

When the flow starts we should mark the Metering invoice in the distributed map as 'ABOUT_TO_ISSUE' - this will act as a softlock to stop it being selected when the cluster leadership changes.

If the flow completes we update the metering invoice state in the Distributed map as ‘ISSUED'
if it fails, then we log out the error and move to the next metering invoice.

If the node goes offine halfway through the flow, can we make the assumption that the flow will recover and complete - this does mean that we should update the distributed map in the flow (after wait for ledger commit).

### What we need for approach 1:

1. ‘Raft’ specific implementation of the AbstractMeterer with hooks into the RaftUniqueness provider - we may have to make the MeteringService a fully fledged ‘Notary’ Service - i.e. derived from it.
2. New ‘Metering’ distributed map to hold the new ‘Unmetered’ metering invoices.
3. ‘Listener hook’ into RaftNotary service so that we can listen for new metering invoices or 'commit response' hook from the client ‘commit’ function, so that we can add the new metering invoices when all cluster nodes have responded.
4. Metering invoice is changed so that it can hold an array of metering node identies (so that they all get paid when the invoice is dispersed)
5. Depending on the 2) above, we may need to add the identity of the node that created the transaction so that we can create the metering invoice.
6. A way of detecting if we are the current Raft Leader - be it via poll or event callback.

### Pros/Cons with approach 1:

#### Pros

1. Works in sync with the Raft Cluster and uses it's functionality - mitigating the need for some other form of metering 'leadership' protocol.
2. Metering can run on any node and can recover from node failure.
3. Minimum changes to Corda Core Code - we only need to add a couple of hooks.
3. Participants in the cluster are rewarded per notarised transaction so it gives them more incentive to be up and running.

#### Cons
1. If a node goes offline forever, then we could potentially be left with a metering invoice stuck in a flow. This could be detected by checking for the 'ABOUT_TO_ISSUE' state and how long that state has remained. And then it could be re-issued after a period of time. If the failed node recovers, then the flow will fail with a duplicate.
2. Metering invoices will only be created on the leader node - meaning that disputing metering invoices will be tricky - i.e. how do we pick the right node? - can a flow be aimed at the cluster? - in theory yes as this could mirror the RaftNotaryFlow. But when which node does the disputed metering invoice GUI go to - in theory this could look at the distributed map. It would have to be the policy of the metering cluster for any node to handle disputes.

## Approach 2 for RAFT Notary - Special node

We create a special ‘Metering Node’ which joins the RAFT Cluster - all this does is listen for ‘committed’ transactions - it does not have to be the leader
It then runs like a standard single node metered - for prod, this could run in HA mode.
This is relatively easy to implement in that we just have a metering service config, with the X500 name of the target node.

### What we need for Approach 2

Same as approach 1, with the exception point 6 as we don't need to be a leader of the cluster to notarise transaction.

### Pros/Cons with Approach 2

### Pros

1. It is simpler to manage disputes as this is a single node.

### Cons

1. This relies on single node for metering which sort of defeats the purpose of having a
Raft Cluster. But as a stepping stone to full availability this might be an option as managing metering disputes could prove to be complex.

## Other Questions / observations on RAFT

When looking at the states of the notaries in the RAFT demo - we need to understand why not all of the states appear on all the nodes - seems like it is not working, but - need to look again. 

## Approach for BFT or other non fully replicated clusters.  

Here are some theoretical thoughts on this as I’ve not looked too deeply at it and also it is changing at the moment, so theory will do for now.

The main issue for metering on BFT is knowing if you are a faulty replica or correct one.
  
You could make the assumption that as the ‘end’ of notary flow that the request is valid.
Unless the malicious notary was colluding with the client, in which case the replicas would reject the message (in theory)
If the malicious notary randomly created a transaction state and sent it to a client - it would be rejected as the client would not find the transaction in it’s vault (the metering flows deal with this).

Conclusion is then that the metering invoice can be created when the replicas come back with enough replies.

Like Raft - we have two choices:
1) run metering on a single node and listen to events - use that node as the single dispute node.
2) run metering on all nodes where the metering state is persisted and ‘disputed’ on that notary this would work in that the node owner is the producer of the metering invoice.

We could also consider in both cases how we would propagate the state to all nodes for subsequent disputes - this could be handled as part of the Issue Metering invoice flow.

Questions:

Is there a way of telling if you are a faulty replica? - or does this even matter?

## Distributed metering utopia on notary clusters.

If we had no restriction over specific technology implementations this is how 

Ideally metering invoices would be distributed to all nodes.
If these metering invoices were disputed, then any of the nodes could respond - in theory we can do this by making each present ‘notary’ members of the metering invoice (we can do this in theory with BFT for those that correctly return the notary transactions) - i.e stamp it with all the notaty nodes that were present when the invoice was raised - if a notary drops out the pool it does not matter.

i.e. you could do this 
Notary (flow) receives transaction
Transaction is sent to cluster
wait for enough Qurom in the respondants
gather the public keys/identieis of the respondents
create a metering invoice - add all respondants + target - could the owner be a composite key?
if failed on one respond - remove and try again
business node can now dispute to any of the participants.

How do dispersals work?

All participants (other than the client) are paid a percentage split of the fee.
This makes it even more fun if the guardian notaries are also a pool - the split would have to go to each member of that pool as well (same pattern as the original invoice)

So this means that all the 'well behaving nodes' must be on the participant list of the metering invoice.


# Design proposal for notary transaction api and other enablers for metering

## TL;DR

This design doc contains the following:

* A proposal for  new notary transaction api to extract notarised transactions in a standard format.
* A number of questions on how this api should be surfaced while maintaining security.
* Notarise method to support meta data to enable prioritisation of transactions.
* Notary and Node blacklisting / whitelisting to enable tight control over usage and connectivity for notaries and business networks. 

## Introduction

Metering is a method of incentivising organisations to run a notary by enabling them to charge for their use on a transaction by
transaction basis.

A detailed explaination of why, what and how metering is implemented in cordite can be found [here]([here](https://medium.com/corda/cordites-metering-notaries-a-deeper-dive-8803d6af269a)) 

## Breaking down the metering process

In the current cordite metering implementation, the process is split into the following stages:

* Extraction of Transactions  

  This is the process and extracting the history of transactions from the notary's commit log. 

* Billing
  
  This is the process of sending an invoice to a Corda node that has used the notary for metering.
  
* Payment
  
  This is the process by which the Corda node pays the invoice for the metered transaction(s).
  
* Fee Distribution / Dispersal

  This is the process by which the fees paid for metering are distributed amongst the metering parties which would include the original 
  notary and potentially other funds which are used for the maintenance of the network infrastructure and software.
  
* Disputes

  This is the process by which the parties involve in a metering transaction can dispute incorrect notarisation, billing or fee distribution.
  
  
Each of the above processes could be implemented in a number of different ways to suit the use cases for particular
business networks or notary clusters. Therefore it is important that we are able to vary these processes independently.

There is an implementation of metering in Cordite that currently performs all of the above for single node validating or
non-validating notaries. A future requirement for metering is to support notary clusters and the ability to support multiple 
business networks.

Currently Cordite metering directly accesses the notary commit log table : NODE_NOTARY_COMMIT_LOG
Long term, this is not a sustainable approach as the underlying store and data structure for the notary transactions
may vary for different notary implementations.

So, the first step is to make a consistent way of extracting transactions from the notary commit log with an Api.

## The Notary Transaction Api

As discussed with the R3 notary team (Rick, Mark, Andrius) We came to consensus (sorry) on the following fields:

### Api Input:

* lastTransactionTime

  This will extract all the notary transactions greater than equal to the input time which is specified in UTC time.
  
  Note: Instead of using transaction time, we did discuss using a sequence number but decided that this would be
  more difficult to implemented, particularly in a BFT scenario. However, we are open to this approach if
  achievable.
  
### Api Outputs:

* transactionIdHash

   The hash of the transactionId is used here to prevent following of the transaction chain.
   
* transactionTime

   The UTC time of the transaction. This will be used by the consumers of the Api to track how far they have got - we would expect them
   to store the last transaction value in their own vaults or databases and then use this value to extract the next 'batch' of notarised transactions. 
   
* abstractParty
 
  The party that instigated the transaction, so that we know where to send the metering invoice.
  
  There is no need to identify the party by name here, unless it is required by the 'rules' of a notary cluster.

* transactionSignature
 
  The instigating node will use this to verify that it did actually create the transaction in the first instance.
  The current metering process checks that the transactionId is in its vault which works fine, but does give away the transaction tree
  to the notary - hence using the hash of the transactionId above.

* Transaction metadata - this is Key/Value map of optional data that can be used by metering services to charge appropriately for the transaction such as :
    - numberOfStates
    
      This gives the billing 'engine' a rough size of the transaction and potentially it may charge more for large transactions
      
    - Transaction Priority
    
      We may introduce notaries that can process transactions according to priority and be rewarded with higher transaction fees.
      
    - transactionType (i.e. is it a notary transfer)
        
      Notaries transfers can potentially be expensive and so the billing engine could charge more for these
    
    - numberOfStatesResolved
     
      In the case of validating notaries the number of states pulled in a notary transfer could have significant impact on the 
      notary and therefore be expensive.
      
    - retries
    
      It is possible that there will be duplicate transactions held in the notary as the result of transaction retries. 
      The billing service may choose to bill for these if there are excessive to prevent DDoS style behaviour.

     Note: We have also discussed having a list of all the signers of a transaction for clustered notaries, so that fees can be 
     selectively distributed to notaries that actually took part in the transaction. This idea has been dismissed as its quite heavyweight,
     and we'd have to wait for all signers slowing down notarisation.  
     
## How will the Notary Transaction api be accessed - current design questions

* If metering runs as a service inside the notary:  Will notary operators be happy to do so?
  Note: this does have the greatest bandwidth.
* If it runs via RPC: Do we then need to expose the Notary RPC to the internet and make sure RPC users are permissioned appropriately?
  or set up some additional firewall rules for Notaries to allow the metering service access?
* Could we offer via P2P?
    i.e. like other Notary flows (primarily the one to notarise), we make it an initiated flow with a client flow for the metering service to invoke.
    We take advantage of visibility of the P2P port (Corda Firewall etc etc), and all other versioning and compatibility work.
*  Could we do this with a non persistent artemis queue?
   - i.e. the notary just streams the transactions via a queue to the listener after it has sent the initial request to ‘get transactions from ‘this’ date'
  
  
## Challenges for Metering / billing in a clustered environment

The cordite metering service currently runs the billing operation for a single node notary.

We need further design work to flesh out how the metering service will run with clustered notaries.

Current thoughts are that:
1. The metering service will collect transactions from a quorum of the notary cluster until it is happy that the transaction is valid
 
    Note: the ‘invoice’ for the transaction can ultimately be rejected by the original node if it does not recognise it, so the ‘quorum’ can be set to a ‘reasonable’ number.
    
2. The metering service can only run one instance at a time, so could operate in a round robin or leadership election manner in the cluster
   or it could just be an external service running in its own process.
3. Metering invoices need to be recorded in a vault and have the capability to be disputed, so we must be mindful of this in the overall design.
   Current thoughts on this are that disputes can go to any node in the cluster which can then verify the validity of the invoice by checking the signature and
   getting a quorum from the other nodes in the cluster. We also have to consider that the original notary that created the metering invoice may
   be temporarily offline or have left the cluster and so other member of the cluster should be able to pick it up. Lastly if a metering notary is leaving the cluster
   we should make sure that any outstanding invoices are either completed or delegated to another member of the cluster. 
 
## Challenges for fee distribution in a clustered environment

Once metering invoices have been paid, the fees then need to be dispersed to each notary in the metering pool. 
We decided that recording the signatures of all notaries in the pool on each transaction would be ‘expensive’ in terms of
data and network traffic even if in theory we could distribute fees at this granular level.
There a number of more coarse grained approaches to check that notaries of a cluster are contributing positively, such as periodically
checking availability. As above, fee distribution is a separate concern to billing which gives business networks the flexibility to create their own mechanisms.

## Notary Transaction Meta Data to improve billing opportunities

In order to provide a more flexible notarisation and metering model, giving the ability for the ‘client’ node to put meta data on the ’Notarise’ method would enable:
	* Transactions to be prioritised for processing - potentially for a greater metering fee.
	* Transaction importance can be specified - i.e. high value transactions which carry financial risk could be highlighted, so that the notary knows the risk they are undertaking
        Note: we are throwing this 'importance' as an idea to see if it resonates with anyone.

So we propose that we add some extra parameters to the ‘notarise’ function for these values to specified i.e.
	* priority (ushort) - 0 no priority - 65535 - max priority
	* weight (ushort) - 0 no weight - 65535 - highly valuable

These parameters could be hardwired or be part of a key/value pair input - comments welcome!

## The need for Notary and Node Blacklisting / Whitelisting

Running metering as a Cordapp gives us the flexibility to decide what flavour of metering we want, if at all.
The current Cordite metering implementation uses all the great stuff from corda (states, flows, transactions, services) to
secure and create trustworthy billing.
The downside of this of course is that the client node has to have the cordite libraries for this to work.
This then means if you are in the same compatibility zone, you can use any notary you fancy and get free transactions if
you refuse to run the metering libraries. 
Another case is that even though a node has a metering invoice, she can choose not to pay it.

In order to prevent nodes from using you as a notary, we need to have a flexible mechanism to
    * blacklist - i.e. refuse transactions from certain nodes/parties if they are behaving badly
    * whitelist - i.e. only allow notarisation for a pre-approved list of nodes/parties 

Business networks may also want to control who connects to nodes in their network - so again blacklisting/whitelisting
would also be a useful feature.

For the next steps I will raise 3 issues:
1. proposal for the notary transaction api
2. proposal for whitelisting / blacklisting
3. proposal for notarise meta-data

all comments and suggestions welcome!




