<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
# Design Note and Questions on DGL

## Notes

1. Promissory Notes are a formal instrument as described [here](https://www.investopedia.com/terms/p/promissorynote.asp)
2. Note the properties that can be encoded into a note: principle, **interest**, **maturity date**


## Questions

1. Are we building our own UI? Or are we trying to integrate with an existing UI? (c.f. Julian)
2. Promissory notes have several fields that are not required for fund transfers. e.g. maturity date. Fund transfers sound
like Demand Promissory Notes, where there is no maturity date. Also no interest accrues on these.  