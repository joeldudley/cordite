/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite

import net.corda.core.identity.CordaX500Name
import net.corda.core.utilities.getOrThrow
import net.corda.testing.driver.DriverParameters
import net.corda.testing.driver.driver
import net.corda.testing.node.User

fun main(args: Array<String>) {
  // No permissions required as we are not invoking core.
  val user = User("user1", "test", permissions = setOf())
  driver(DriverParameters(isDebug = true, waitForAllNodesToFinish = true, startNodesInProcess = true)) {
    listOf(
        // removed advertised services - not sure how to put that back in...
//        startNode(providedName = CordaX500Name("Controller", "London", "GB")),
        startNode(providedName = CordaX500Name("PartyA", "London", "GB"), rpcUsers = listOf(user)),
        startNode(providedName = CordaX500Name("PartyB", "New York", "US"), rpcUsers = listOf(user))
    ).map { it.getOrThrow() }
  }
}

