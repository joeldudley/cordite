/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.service

import io.cordite.dao.DaoApiImpl
import io.cordite.metering.daostate.MeteringModelData
import io.cordite.metering.daostate.MeteringNotaryMember
import net.corda.core.node.AppServiceHub
import net.corda.core.utilities.loggerFor

class MeteringDaoStateLoader(serviceHub : AppServiceHub,val daoName: String) {

  private val log = loggerFor<MeteringDaoStateLoader>()
  private val daoService = DaoApiImpl(serviceHub)
  private var meteringModelData : MeteringModelData? =null


  init {
    log.info("Metering Dao State Loader Created")
  }

  fun getMeteringDao() : MeteringModelData? {
    log.info("trying to get dao list for '$daoName'")
    val daoStates = daoService.daoInfo(daoName = daoName)

    if(daoStates.isNotEmpty()){
      log.info("${daoStates.count()} daoState(s) Loaded")

      daoStates.forEach {
        val potentialMeteringModel = it.get(MeteringModelData::class)

        if(potentialMeteringModel !=null){
          meteringModelData = potentialMeteringModel
          log.info("Metering Model Data Loaded")
          return meteringModelData
        }
      }
    }
    return null
  }

  fun getMeteringNotariesFromDao() : List<MeteringNotaryMember> {

    // accumulate a union set of all metering notaries across all reported Dao states
    //var setOfNotaries: MutableSet<MeteringNotaryMember> = mutableSetOf<MeteringNotaryMember>()
    var setOfNotaries: Set<MeteringNotaryMember> = mutableSetOf()

    val daoStates = daoService.daoInfo(daoName = daoName)

    daoStates.forEach {
      val mm: MeteringModelData = it.get(MeteringModelData::class)!!
      setOfNotaries = setOfNotaries.plus(mm.meteringNotaryMembers.values)
    }

    return setOfNotaries.toList()
  }
}