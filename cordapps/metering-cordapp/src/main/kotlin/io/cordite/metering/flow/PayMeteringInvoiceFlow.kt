/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.flow

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dgl.corda.account.AccountAddress
import io.cordite.dgl.corda.token.TokenType
import io.cordite.dgl.corda.token.flows.TransferTokenSenderFunctions.Companion.prepareTokenMoveWithSummary
import io.cordite.metering.contract.*
import io.cordite.metering.schema.MeteringInvoiceSchemaV1
import net.corda.core.contracts.*
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.node.services.Vault
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.node.services.vault.builder
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import net.corda.core.utilities.ProgressTracker.Step
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.math.BigDecimal
import java.security.PublicKey
import java.time.Instant

object PayMeteringInvoiceFlow {

  private val log: Logger = LoggerFactory.getLogger(PayMeteringInvoiceFlow.javaClass)

  @InitiatingFlow
  @StartableByRPC
  @StartableByService
  class MeteringInvoicePayer(val payMeteringInvoiceRequest: MeteringInvoiceFlowCommands.PayMeteringInvoiceRequest) : FlowLogic<SignedTransaction>() {
    companion object {
      object BUILD_METERING_INVOICE_TRANSACTION : Step("Build the metering invoice transaction")
      object VERIFYING_TRANSACTION : Step("Verifying contract constraints.")
      object SIGNING_TRANSACTION : Step("Signing transaction with our private key.")
      object GATHERING_SIGS : Step("Gathering the counterparty's signature.") {
        override fun childProgressTracker() = CollectSignaturesFlow.tracker()
      }
    }

    object FINALISING_TRANSACTION : Step("Obtaining notary signature and recording transaction.") {
      override fun childProgressTracker() = FinalityFlow.tracker()
    }

    override val progressTracker = ProgressTracker(
      BUILD_METERING_INVOICE_TRANSACTION,
      VERIFYING_TRANSACTION,
      SIGNING_TRANSACTION,
      GATHERING_SIGS,
      FINALISING_TRANSACTION
    )

    @Suspendable
    override fun call(): SignedTransaction {

      val originalMeteringInvoice = getOriginalMeteringInvoice()
      val txBuilder = buildTransaction(originalMeteringInvoice)
      val tokenSiginingKeys = addPaymentTransaction(originalMeteringInvoice.state.data.meteringInvoiceProperties, txBuilder)

      val partiallySignedTx = verifyAndSignTransaction(txBuilder, tokenSiginingKeys + serviceHub.myInfo.legalIdentities.first().owningKey)
      val signatureParties = getSignatureParties(originalMeteringInvoice)
      val fullySignedTx = getSignatureFromReceiver(partiallySignedTx,signatureParties)
      val finalisedTx = finaliseTransaction(fullySignedTx)
      return finalisedTx
    }

    fun buildTransaction(originalMeteringInvoiceStateAndRef :StateAndRef<MeteringInvoiceState>): TransactionBuilder {
      progressTracker.currentStep = BUILD_METERING_INVOICE_TRANSACTION

      val originalMeteringInvoiceProperties = originalMeteringInvoiceStateAndRef.state.data.meteringInvoiceProperties

      val meteringInvoiceProperties = MeteringInvoiceProperties(
        meteringState = MeteringState.PAID,
        invoicedParty = originalMeteringInvoiceProperties.invoicedParty,
        invoicingNotary = originalMeteringInvoiceProperties.invoicingNotary,
        daoParty = originalMeteringInvoiceProperties.daoParty,
        payAccountId = originalMeteringInvoiceProperties.payAccountId,
        amount = originalMeteringInvoiceProperties.amount,
        tokenDescriptor = originalMeteringInvoiceProperties.tokenDescriptor,
        meteredTransactionId = originalMeteringInvoiceProperties.meteredTransactionId,
        invoiceId = originalMeteringInvoiceProperties.invoiceId,
        reissueCount = originalMeteringInvoiceProperties.reissueCount,
        createdDateTime = Instant.now())

      val notary = originalMeteringInvoiceStateAndRef.state.notary
      val meteringInvoiceStateToIssue = MeteringInvoiceState(meteringInvoiceProperties,originalMeteringInvoiceProperties.daoParty)
      val txCommand = Command(MeteringInvoiceCommands.Pay(), meteringInvoiceStateToIssue.participants.map { it.owningKey })

      val txBuilder = TransactionBuilder(notary).withItems(StateAndContract(meteringInvoiceStateToIssue, MeteringInvoiceContract.METERING_CONTRACT_ID), txCommand)
      txBuilder.addInputState(originalMeteringInvoiceStateAndRef)

      return txBuilder
    }

    fun addPaymentTransaction(meteringInvoiceProperties: MeteringInvoiceProperties, txBuilder: TransactionBuilder) : List<PublicKey> {
      val fromAddress = AccountAddress(payMeteringInvoiceRequest.fromAccount,meteringInvoiceProperties.invoicedParty.name)
      val toAddress = AccountAddress(meteringInvoiceProperties.payAccountId,meteringInvoiceProperties.daoParty.name)
      val tokenType = TokenType.Descriptor(meteringInvoiceProperties.tokenDescriptor.symbol,meteringInvoiceProperties.tokenDescriptor.exponent, meteringInvoiceProperties.tokenDescriptor.issuerName)

        val amountInTokenType = Amount.fromDecimal(BigDecimal(meteringInvoiceProperties.amount), tokenType)
      val tokenSigingKeys = prepareTokenMoveWithSummary(txBuilder,fromAddress,toAddress,amountInTokenType,serviceHub, ourIdentity, "metering invoice flow - payment transaction")
      return tokenSigingKeys
    }

    fun getSignatureParties(originalMeteringInvoice: StateAndRef<MeteringInvoiceState>) : List<Party>{
      return listOf(originalMeteringInvoice.state.data.meteringInvoiceProperties.daoParty,originalMeteringInvoice.state.data.meteringInvoiceProperties.invoicingNotary)
    }

    private fun getOriginalMeteringInvoice(): StateAndRef<MeteringInvoiceState> {
      val result = builder {
        val expression = MeteringInvoiceSchemaV1.PersistentMeteringInvoice::meteredTransactionId.equal(payMeteringInvoiceRequest.meteredTransactionId)
        val customCriteria = QueryCriteria.VaultCustomQueryCriteria(expression)
        val generalCriteria = QueryCriteria.VaultQueryCriteria(Vault.StateStatus.ALL)
        val fullCriteria = generalCriteria.and(customCriteria)
        serviceHub.vaultService.queryBy<MeteringInvoiceState>(fullCriteria).states.last()
      }
      return result
    }

    @Suspendable
    fun verifyAndSignTransaction(txBuilder: TransactionBuilder, signingPubKeys: Iterable<PublicKey>): SignedTransaction {
      progressTracker.currentStep = VERIFYING_TRANSACTION
      txBuilder.verify(serviceHub)
      progressTracker.currentStep = SIGNING_TRANSACTION
      val partiallySignedTx = serviceHub.signInitialTransaction(txBuilder, signingPubKeys.distinct())
      return partiallySignedTx
    }

    @Suspendable
    fun getSignatureFromReceiver(partiallySignedTx: SignedTransaction, parties : List<Party>): SignedTransaction {
      progressTracker.currentStep = GATHERING_SIGS

      val flowSessions = parties.map { initiateFlow(it) }

      val fullySignedTx = subFlow(CollectSignaturesFlow(partiallySignedTx, flowSessions.toSet(), GATHERING_SIGS.childProgressTracker()))
      return fullySignedTx
    }

    @Suspendable
    fun finaliseTransaction(fullySignedTx: SignedTransaction): SignedTransaction {
      progressTracker.currentStep = FINALISING_TRANSACTION
      val finalisedTx = subFlow(FinalityFlow(fullySignedTx, FINALISING_TRANSACTION.childProgressTracker()))
      return finalisedTx
    }
  }

  @InitiatedBy(MeteringInvoicePayer::class)
  class MeteringInvoicePayee(val otherPartyFlow: FlowSession) : FlowLogic<SignedTransaction>() {
    @Suspendable
    override fun call(): SignedTransaction {
      val signTransactionFlow = object : SignTransactionFlow(otherPartyFlow) {
        override fun checkTransaction(stx: SignedTransaction) = requireThat {
          //TODO - add check for payment
          val output = stx.tx.outputs.filter { it.data is MeteringInvoiceState }.single().data
          val meteringInvoiceState = output as MeteringInvoiceState
          "check that this this is a valid transaction we are being metered for" using (MeteringInvoiceFlowChecks.isTheMeteredTransactionValid(meteringInvoiceState, serviceHub, log))
        }
      }
      return subFlow(signTransactionFlow)
    }
  }
}
