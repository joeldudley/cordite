/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.cordite.metering.flow

import io.cordite.dgl.corda.token.TokenType
import io.cordite.metering.contract.MeteringInvoiceState
import io.cordite.metering.schema.MeteringInvoiceSchemaV1
import net.corda.core.contracts.StateAndRef
import net.corda.core.identity.Party
import net.corda.core.node.ServiceHub
import net.corda.core.node.services.Vault
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.node.services.vault.builder
import org.slf4j.LoggerFactory

class FlowUtils {

  companion object
  {
    fun getOriginalMeteringInvoices(serviceHub: ServiceHub, meteredTransactionIds: List<String>): List<StateAndRef<MeteringInvoiceState>> {

      val result = builder {
        val expression = MeteringInvoiceSchemaV1.PersistentMeteringInvoice::meteredTransactionId.`in`(meteredTransactionIds)
        val customCriteria = QueryCriteria.VaultCustomQueryCriteria(expression)
        val generalCriteria = QueryCriteria.VaultQueryCriteria(Vault.StateStatus.ALL)
        val fullCriteria = generalCriteria.and(customCriteria)
        serviceHub.vaultService.queryBy<MeteringInvoiceState>(fullCriteria).states
      }
      return result
    }

    fun getOriginalMeteringInvoice(serviceHub: ServiceHub, meteredTransactionId: String): StateAndRef<MeteringInvoiceState> {
      val result = builder {
        val expression = MeteringInvoiceSchemaV1.PersistentMeteringInvoice::meteredTransactionId.equal(meteredTransactionId)
        val customCriteria = QueryCriteria.VaultCustomQueryCriteria(expression)
        val generalCriteria = QueryCriteria.VaultQueryCriteria(Vault.StateStatus.ALL)
        val fullCriteria = generalCriteria.and(customCriteria)
        serviceHub.vaultService.queryBy<MeteringInvoiceState>(fullCriteria).states.last()
      }
      return result
    }

  }
}

/**
 * extracts attributes from a list of metering invoices, checking that they have the same value for all supplied invoices
 */
class MeteringInvoiceConsistencyChecker (val originalInvoices: List<StateAndRef<MeteringInvoiceState>>) {

  fun getInvoicedParty(): Party {
    val logger = LoggerFactory.getLogger("MeteringInvoiceConsistencyChecker")
    logger.info("getInvoiceParty...")
    originalInvoices.forEach { logger.info(it.toString()) }

    try {
      val invoicedParty = originalInvoices.groupBy { it.state.data.meteringInvoiceProperties.invoicedParty }.asSequence().single().key;
      return invoicedParty
    } catch (e: Exception) {
      throw IllegalArgumentException("every invoice must be for the same invoiced party", e)
    }
  }

  fun getInvoiceNotary(): Party {
    try {
      val invoiceNotary = originalInvoices.groupBy { it.state.data.meteringInvoiceProperties.invoicingNotary }.asSequence().single().key
      return invoiceNotary
    } catch (e: Exception) {
      throw IllegalArgumentException("every invoice must be notarised by the same notary", e)
    }
  }

  fun getNotarisingNotary(): Party {
    try {
      val notarisingNotary = originalInvoices.groupBy { it.state.notary }.asSequence().single().key
      return notarisingNotary
    } catch (e: Exception) {
      throw IllegalArgumentException("the invoice payments must be notarised by the same notary", e)
    }
  }

  fun getDaoParty(): Party {
    try {
      val daoParty = originalInvoices.groupBy { it.state.data.meteringInvoiceProperties.daoParty }.asSequence().single().key
      return daoParty
    } catch (e: Exception) {
      throw IllegalArgumentException("every invoice must have the same dao", e)
    }
  }

  fun getPayAccount(): String {
    try {
      val payAccount = originalInvoices.groupBy { it.state.data.meteringInvoiceProperties.payAccountId }.asSequence().single().key
      return payAccount
    } catch (e: Exception) {
      throw IllegalArgumentException("every invoice must have the same to address", e)
    }
  }

  fun getTokenType(): TokenType.Descriptor {
    try {
      val tokenType = originalInvoices.groupBy { it.state.data.meteringInvoiceProperties.tokenDescriptor }.asSequence().single().key
      return tokenType
    } catch (e: Exception) {
      throw IllegalArgumentException("every invoice must be denominated in the same token currency", e)
    }
  }
}

/**
 * extracts attributes from a list of metering invoices, checking that they have the same value for all supplied invoices
 */
class IssueMeteringInvoiceRequestConsistencyChecker (val requests: List<MeteringInvoiceFlowCommands.IssueMeteringInvoiceRequest>) {

  fun getInvoicedParty(): Party {
    val logger = LoggerFactory.getLogger("IMIRCC")
    logger.info("getInvoiceParty...")
    requests.forEach { logger.info(it.toString()) }

    try {
      val invoicedParty = requests.groupBy { it.invoicedParty }.asSequence().single().key;
      return invoicedParty
    } catch (e: Exception) {
      throw IllegalArgumentException("every invoice must be for the same invoiced party", e)
    }
  }

  fun getInvoiceNotary(): Party {
    try {
      val invoiceNotary = requests.groupBy { it.invoicingNotary }.asSequence().single().key
      return invoiceNotary
    } catch (e: Exception) {
      throw IllegalArgumentException("every invoice must be notarised by the same notary", e)
    }
  }

  fun getNotarisingNotary(): Party {
    try {
      val notarisingNotary = requests.groupBy { it.meteringPolicingNotary }.asSequence().single().key
      return notarisingNotary
    } catch (e: Exception) {
      throw IllegalArgumentException("the invoice payments must be notarised by the same notary", e)
    }
  }

  fun getDaoParty(): Party {
    try {
      val daoParty = requests.groupBy { it.daoParty }.asSequence().single().key
      return daoParty
    } catch (e: Exception) {
      throw IllegalArgumentException("every invoice must have the same dao", e)
    }
  }

  fun getPayAccount(): String {
    try {
      val payAccount = requests.groupBy { it.payAccountId }.asSequence().single().key
      return payAccount
    } catch (e: Exception) {
      throw IllegalArgumentException("every invoice must have the same to address", e)
    }
  }

  fun getTokenType(): TokenType.Descriptor {
    try {
      val tokenType = requests.groupBy { it.tokenDescriptor }.asSequence().single().key
      return tokenType
    } catch (e: Exception) {
      throw IllegalArgumentException("every invoice must be denominated in the same token currency", e)
    }
  }
}


