/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.testutils

import io.cordite.dao.DaoApiImpl
import io.cordite.dao.core.DaoState
import io.cordite.dao.proposal.ProposalLifecycle
import io.cordite.dgl.corda.impl.LedgerApiImpl
import io.cordite.dgl.corda.token.TokenType
import io.cordite.metering.daostate.*
import io.cordite.test.utils.TempHackedAppServiceHubImpl
import net.corda.core.identity.Party
import net.corda.core.utilities.loggerFor
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.StartedMockNode
import org.junit.Assert

class MeteringDaoSetup {
  companion object {

    private val log = loggerFor<MeteringDaoSetup>()

    fun setUpDaoAndMeteringData(daoName: String,
                                daoHoldingAccount: String,
                                daoFoundationAccount : String,
                                daoNode: StartedMockNode,
                                meteringNotary: StartedMockNode,
                                guardianNotary: StartedMockNode,
                                bootstrapNotary : StartedMockNode,
                                mockNet: MockNetwork,
                                meteringNotaryAccountId : String,
                                guardianNotaryAccountId: String) : MeteringModelData {

      val guardianNotaryParty = guardianNotary.info.legalIdentities.first()
      val meteringNotaryParty = meteringNotary.info.legalIdentities.first()
      val daoNodeParty = daoNode.info.legalIdentities.first()
      val bootstrapNotaryParty = bootstrapNotary.info.legalIdentities.first()

      val daoNodeWithDaoService = DaoApiImpl(TempHackedAppServiceHubImpl(daoNode))
      val meteringNodeWithDaoService = DaoApiImpl(TempHackedAppServiceHubImpl(meteringNotary))
      val guardianNodeWithDaoService = DaoApiImpl(TempHackedAppServiceHubImpl(guardianNotary))

      val daoState = io.cordite.test.utils.run(mockNet) { daoNodeWithDaoService.createDao(daoName,bootstrapNotaryParty.name) }

      val guardianNotaryMemberProposal = io.cordite.test.utils.run(mockNet) { guardianNodeWithDaoService.createNewMemberProposal(daoState.name, daoNodeParty.name) }

      // accept member proposal
      io.cordite.test.utils.run(mockNet) { guardianNodeWithDaoService.sponsorAcceptProposal(guardianNotaryMemberProposal.proposal.key(), daoState.daoKey, daoNodeParty.name) }

      val meteringNotaryMemberProposal = io.cordite.test.utils.run(mockNet) { meteringNodeWithDaoService.createNewMemberProposal(daoState.name, daoNodeParty.name) }

      io.cordite.test.utils.run(mockNet) { guardianNodeWithDaoService.voteForProposal(meteringNotaryMemberProposal.proposal.key()) }

      // accept member proposal
      val acceptedProposalLifecycle = io.cordite.test.utils.run(mockNet) { meteringNodeWithDaoService.sponsorAcceptProposal(meteringNotaryMemberProposal.proposal.key(), daoState.daoKey, daoNodeParty.name) }
      Assert.assertEquals("proposal should be accepted", ProposalLifecycle.ACCEPTED, acceptedProposalLifecycle)

      val daoStateWithAllNotaries = meteringNodeWithDaoService.daoFor(daoState.daoKey)

      assert(guardianNotaryParty in daoStateWithAllNotaries.members)
      assert(meteringNotaryParty in daoStateWithAllNotaries.members)

      //Create the Metering Dao State
      val meteringDaoModel = buildMeteringModelData(meteringNotaryParty, guardianNotaryParty, daoNodeParty, daoHoldingAccount, daoFoundationAccount, meteringNotaryAccountId, guardianNotaryAccountId)

      val meteringModelProposalState = io.cordite.test.utils.run(mockNet) { daoNodeWithDaoService.createModelDataProposal("MeteringParameters", meteringDaoModel, daoState.daoKey) }

      io.cordite.test.utils.run(mockNet) { guardianNodeWithDaoService.voteForProposal(meteringModelProposalState.proposal.key()) }
      io.cordite.test.utils.run(mockNet) { meteringNodeWithDaoService.acceptProposal(meteringModelProposalState.proposal.key()) }

      //Lets get the dao state and check
      val daoStates = getDaoWithRetry(daoNodeWithDaoService, daoName)

      val meteringModelData = daoStates.first().get(MeteringModelData::class) as MeteringModelData

      assert(meteringModelData.meteringNotaryMembers.count() == 2)
      assert(meteringModelData.meteringFeeAllocation.daoHoldingAccountId == daoHoldingAccount)
      assert(meteringModelData.meteringFeeAllocation.daoFoundationAccount == daoFoundationAccount)
      assert(meteringModelData.meteringTransactionCost.meteringTransactionTokenDescriptor.symbol == "XTS")

      return meteringModelData;
    }

    fun buildMeteringModelData(meteringNotaryParty: Party, guardianNotaryParty: Party, daoParty: Party, daoHoldingAccount: String, daoFoundationAccount: String, meteringNotaryAccountId : String, guardianNotaryAccountId : String): MeteringModelData {
      val meteringNotaryMember = MeteringNotaryMember(meteringNotaryParty, null, meteringNotaryAccountId, "I am a cool metering notary", MeteringNotaryType.METERER)
      val guardianNotaryMember = MeteringNotaryMember(guardianNotaryParty, null, guardianNotaryAccountId, "I am a guardian of the galaxy", MeteringNotaryType.GUARDIAN)

      val meteringFeeAllocation = MeteringFeeAllocation(daoHoldingAccount, 50, 40, 10, daoFoundationAccount)
      val meteringTransactionCost = MeteringTransactionCost(10, TokenType.Descriptor("XTS", 2, daoParty.name))
      val meteringNotaryMembers = mapOf(meteringNotaryMember.notaryParty.name.toString() to meteringNotaryMember, guardianNotaryMember.notaryParty.name.toString() to guardianNotaryMember)

      val meteringDaoModel = MeteringModelData(meteringTransactionCost, meteringNotaryMembers, meteringFeeAllocation)
      return meteringDaoModel
    }

    fun getDaoWithRetry(testNode: DaoApiImpl, daoName: String): List<DaoState> {
      (1..5).forEach {
        log.info("trying to get dao list")
        val daos = testNode.daoInfo(daoName)
        if (daos.isNotEmpty()) {
          log.info("phew - daos returned")
          return daos
        }
        log.info("dao state not arrived yet - snoozing")
        Thread.sleep(100)
      }
      throw RuntimeException("Unable to get daos from node - are you sure this is a race condition?")
    }
  }
}