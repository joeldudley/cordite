/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.service

import io.cordite.metering.daostate.MeteringNotaryMember
import io.cordite.metering.daostate.MeteringNotaryType
import net.corda.core.crypto.generateKeyPair
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class RandomNotarySelectorTests{

  val guardianNotaryKeys = generateKeyPair()
  val guardianNotaryParty = Party(CordaX500Name("GuardianNotary", "Argleton", "GB"), guardianNotaryKeys.public)

  @Test
  fun `I can select a guardian notary at random for fairness and equality`(){
    val guardianList = mutableListOf<MeteringNotaryMember>()

    val guardianNotary1 = MeteringNotaryMember(guardianNotaryParty, null, "guardian-notary-acc1", "Rocket Racoon", MeteringNotaryType.GUARDIAN)
    val guardianNotary2 = MeteringNotaryMember(guardianNotaryParty, null, "guardian-notary-acc2", "Skylord", MeteringNotaryType.GUARDIAN)
    val guardianNotary3 = MeteringNotaryMember(guardianNotaryParty, null, "guardian-notary-acc3", "Gamora", MeteringNotaryType.GUARDIAN)
    val meteringNotary4 = MeteringNotaryMember(guardianNotaryParty, null, "guardian-notary-acc4", "Dave", MeteringNotaryType.METERER)
    val meteringNotary5 = MeteringNotaryMember(guardianNotaryParty, null, "guardian-notary-acc5", "Drax", MeteringNotaryType.METERER)

    val approvedNotaries = listOf(guardianNotary1, guardianNotary2, guardianNotary3, meteringNotary4, meteringNotary5)

    val notarySelections: MutableMap<MeteringNotaryMember, Int> = mutableMapOf( Pair(guardianNotary1, 0), Pair(guardianNotary2, 0), Pair(guardianNotary3, 0), Pair(meteringNotary4, 0), Pair(meteringNotary5, 0) )

    for(a in 0..50){
      val randomGuardianNotary = RandomNotarySelector.getRandomMeteringNotary(approvedNotaries, MeteringNotaryType.GUARDIAN)
      guardianList.add(randomGuardianNotary)
      notarySelections.put(randomGuardianNotary, (notarySelections.getValue(randomGuardianNotary)+1))
    }

    //We should have some reasonable distribution
    val numDistinctGuardians = guardianList.distinctBy { it.accountId }.count()
    val numGuardianSelections = guardianList.count()
    assert(numDistinctGuardians != numGuardianSelections)

    // Probability that each guardian notary will be picked at least once in this test is 0.9999999984316714545160413776663173443011013622049516948674767078765302132069128099775770408090721076
    assertTrue(notarySelections.getValue(guardianNotary1) > 0, "guardian notary should have been selected at least once by now!" )
    assertTrue(notarySelections.getValue(guardianNotary2) > 0, "guardian notary should have been selected at least once by now!" )
    assertTrue(notarySelections.getValue(guardianNotary3) > 0, "guardian notary should have been selected at least once by now!" )

    assertEquals(0, notarySelections.getValue(meteringNotary4), "metering notary should never be selected as guardian!" )
    assertEquals(0, notarySelections.getValue(meteringNotary5), "metering notary should never be selected as guardian!" )

  }
}