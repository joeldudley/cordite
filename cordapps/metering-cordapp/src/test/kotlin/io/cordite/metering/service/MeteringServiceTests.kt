/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.service

import io.bluebank.braid.core.async.getOrThrow
import io.cordite.dao.proposal.SponsorProposalFlowResponder
import io.cordite.dao.proposal.VoteForProposalFlowResponder
import io.cordite.dgl.corda.impl.LedgerApiImpl
import io.cordite.dgl.corda.tag.WellKnownTagCategories
import io.cordite.dgl.corda.token.TokenType
import io.cordite.metering.contract.MeteringInvoiceSplitState
import io.cordite.metering.contract.MeteringInvoiceState
import io.cordite.metering.contract.MeteringSplitState
import io.cordite.metering.contract.MeteringState
import io.cordite.metering.flow.IssueMeteringInvoiceFlow
import io.cordite.metering.flow.MeteringInvoiceFlowCommands
import io.cordite.metering.flow.PayMeteringInvoiceFlow
import io.cordite.metering.schema.MeteringInvoiceSchemaV1
import io.cordite.metering.schema.MeteringInvoiceSplitSchemaV1
import io.cordite.metering.testutils.MeteringDaoSetup.Companion.setUpDaoAndMeteringData
import io.cordite.metering.testutils.MeteringRunAndRetry.Companion.runAndRetryWithMockNetworkRefresh
import io.cordite.test.utils.TempHackedAppServiceHubImpl
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.node.services.Vault
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.Builder.equal
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.utilities.getOrThrow
import net.corda.core.utilities.loggerFor
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.MockNetworkNotarySpec
import net.corda.testing.node.StartedMockNode
import org.junit.After
import org.junit.Before
import org.junit.Test

class MeteringServiceTests {

  private val log = loggerFor<MeteringServiceTests>()

  companion object {
    val ACCOUNT_1 = "account-1"
    val ACCOUNT_2 = "account-2"
  }

  private val daoName = "Cordite Committee"

  private lateinit var mockNet: MockNetwork
  private lateinit var bootstrapNotary: StartedMockNode
  private lateinit var meteringNotary: StartedMockNode
  private lateinit var guardianNotary: StartedMockNode
  private lateinit var sector1BankNode: StartedMockNode
  private lateinit var sector2BankNode: StartedMockNode
  private lateinit var daoNode: StartedMockNode
  private lateinit var sector1Bank: Party
  private lateinit var sector2Bank: Party
  private lateinit var daoNodeParty: Party
  private lateinit var meteringNotaryParty: Party
  private lateinit var guardianNotaryParty: Party
  private lateinit var bootstrapNotaryParty: Party
  private lateinit var testTokenXTS: TokenType.State

  private val sector1BankX500Name = CordaX500Name("Sector1Bank", "London", "GB")
  private val meteringNotaryX500Name = CordaX500Name(commonName = "SimpleNotaryService", organisation = "Cordite Metering Notary", locality = "Argleton", country = "GB")
  private val guardianNotaryX500Name = CordaX500Name("Cordite Guardian Notary", "Argleton", "GB")
  private val sector2BankX500Name = CordaX500Name("Sector2Bank", "London", "GB")
  private val daoX500Name = CordaX500Name("Cordite Committee", "London", "GB")
  private val bootstrapNotaryx500Name = CordaX500Name("Cordite Bootstrap Notary", "Wollaston", "GB")

  val testCurrencyXTS = "XTS"
  val sector1BankAccount1 = "sector1-bank-account1"
  val sector2BankAccount1 = "sector2-bank-account1"
  val meteringNotaryAccount1 = "metering-notary-account1"
  val daoHoldingAccount = "dao-holding-account"
  val daoFoundationAccount = "dao-foundation-account"
  val guardianNotaryAccount1 = "guardian-notary-account1"
  val sector1BankAccount2 = "sector1-bank-account2"
  val sector2BankAccount2 = "sector2-bank-account2"
  val daoIssuingAccount = "dao-issuing-account"

  @Before
  fun before() {

    mockNet = MockNetwork(
      cordappPackages = listOf("io.cordite"),
      notarySpecs = listOf(MockNetworkNotarySpec(bootstrapNotaryx500Name, false),
        MockNetworkNotarySpec(guardianNotaryX500Name, true),
        MockNetworkNotarySpec(meteringNotaryX500Name, true)))

    bootstrapNotary = mockNet.notaryNodes.first { it.info.legalIdentities.first().name == bootstrapNotaryx500Name }
    bootstrapNotaryParty = bootstrapNotary.info.legalIdentities.first()

    meteringNotary = mockNet.notaryNodes.first { it.info.legalIdentities.first().name == meteringNotaryX500Name }
    meteringNotary.registerInitiatedFlow(VoteForProposalFlowResponder::class.java)
    meteringNotaryParty = meteringNotary.info.legalIdentities.first()

    guardianNotary = mockNet.notaryNodes.first { it.info.legalIdentities.first().name == guardianNotaryX500Name }
    guardianNotaryParty = guardianNotary.info.legalIdentities.first()

    sector1BankNode = mockNet.createPartyNode(legalName = sector1BankX500Name)
    sector1BankNode.registerInitiatedFlow(IssueMeteringInvoiceFlow.MeteringInvoiceReceiver::class.java)

    sector1Bank = sector1BankNode.info.legalIdentities.first()

    sector2BankNode = mockNet.createPartyNode(legalName = sector2BankX500Name)
    sector2BankNode.registerInitiatedFlow(IssueMeteringInvoiceFlow.MeteringInvoiceReceiver::class.java)

    sector2Bank = sector2BankNode.info.legalIdentities.first()

    daoNode = mockNet.createPartyNode(legalName = daoX500Name)
    daoNode.registerInitiatedFlow(IssueMeteringInvoiceFlow.MeteringInvoiceReceiver::class.java)
    daoNode.registerInitiatedFlow(VoteForProposalFlowResponder::class.java)
    daoNode.registerInitiatedFlow(SponsorProposalFlowResponder::class.java)
    daoNodeParty = daoNode.info.legalIdentities.first()
  }

  @Test
  fun `I'm gonna get an invoiced for creating transactions`() {

    setUpDaoAndMeteringData(daoName, daoHoldingAccount, daoFoundationAccount, daoNode, meteringNotary, guardianNotary, bootstrapNotary, mockNet, meteringNotaryAccount1, guardianNotaryAccount1)

    createTokensAndLoadAccounts()

    val testAssetMoveTransactionId = createAccountsAndDoSomeTransfers()

    Thread.sleep(5000)
    mockNet.runNetwork()

    var retries = 0

    var expectedMeteringInvoice: MeteringInvoiceState? = null

    while (expectedMeteringInvoice == null && retries < 50) {

      sector2BankNode.transaction {
        val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
        val meteringInvoiceStates = sector1BankNode.services.vaultService.queryBy<MeteringInvoiceState>(criteria).states

        meteringInvoiceStates.forEach {
          log.debug("\nTrying to find transaction id $testAssetMoveTransactionId, retry count = $retries")
          val meteredTransactionIdFound = it.state.data.meteringInvoiceProperties.meteredTransactionId
          log.debug("\nCurrently found $meteredTransactionIdFound")
          expectedMeteringInvoice = it.state.data
        }
      }
      Thread.sleep(5000)
      retries += 1

      mockNet.runNetwork()
    }

    assert(expectedMeteringInvoice != null)

    if (expectedMeteringInvoice?.meteringInvoiceProperties?.meteredTransactionId != null) {

      val transactionId = expectedMeteringInvoice?.meteringInvoiceProperties?.meteredTransactionId as String

      //Now Lets pay for the bugger
      val payMeteringInvoiceRequest = MeteringInvoiceFlowCommands.PayMeteringInvoiceRequest(transactionId, sector2BankAccount1)
      val payMeteringInvoiceFlow = PayMeteringInvoiceFlow.MeteringInvoicePayer(payMeteringInvoiceRequest = payMeteringInvoiceRequest)
      val payMeteringInvoiceFlowFuture = sector2BankNode.startFlow(payMeteringInvoiceFlow)

      mockNet.runNetwork()

      payMeteringInvoiceFlowFuture.getOrThrow()
    }

    mockNet.runNetwork()

    val foundMeteringInvoice = expectedMeteringInvoice as MeteringInvoiceState

    //Wait for Dispersed Metering Invoice (Sector1Bank and Metering Notary)
    checkVaultForMeteringInvoice(foundMeteringInvoice, meteringNotary, MeteringState.FUNDS_DISPERSED, mockNet)
    checkVaultForMeteringInvoice(foundMeteringInvoice, daoNode, MeteringState.FUNDS_DISPERSED, mockNet)
    checkVaultForMeteringInvoice(foundMeteringInvoice, sector2BankNode, MeteringState.FUNDS_DISPERSED, mockNet)

    //Wait for and verify for the metering invoice splits - i.e, the divided funds
    checkVaultForMeteringInvoiceSplits(foundMeteringInvoice, guardianNotary, MeteringSplitState.SPLIT_DISPERSED, mockNet)
    checkVaultForMeteringInvoiceSplits(foundMeteringInvoice, daoNode, MeteringSplitState.SPLIT_DISPERSED, mockNet)
    checkVaultForMeteringInvoiceSplits(foundMeteringInvoice, meteringNotary, MeteringSplitState.SPLIT_DISPERSED, mockNet)

    //Check all the payments have made it to their destination
    checkBalance(meteringNotary, meteringNotaryAccount1, 4)
    checkBalance(daoNode, daoFoundationAccount, 5)
    checkBalance(guardianNotary, guardianNotaryAccount1, 1)
  }

  private fun checkBalance(node: StartedMockNode, accountName: String, expectedAmount: Long) {

    val nodeLedger = LedgerApiImpl(TempHackedAppServiceHubImpl(node), { mockNet.runNetwork() })
    val accountBalance = nodeLedger.balanceForAccount(accountName).getOrThrow()

    mockNet.runNetwork()

    assert(accountBalance.first().quantity == expectedAmount * 100)
  }

  private fun checkVaultForMeteringInvoice(expectedMeteringInvoice: MeteringInvoiceState, node: StartedMockNode, meteringState: MeteringState, mockNet: MockNetwork) {
    node.transaction {

      val transactionId = expectedMeteringInvoice.meteringInvoiceProperties.meteredTransactionId

      val expression = MeteringInvoiceSchemaV1.PersistentMeteringInvoice::meteringState.equal(meteringState.name)
      val customCriteria = QueryCriteria.VaultCustomQueryCriteria(expression)
      val generalCriteria = QueryCriteria.VaultQueryCriteria(Vault.StateStatus.ALL)
      val fullCriteria = generalCriteria.and(customCriteria)

      val meteringInvoiceStatesPage = runAndRetryWithMockNetworkRefresh({ node.services.vaultService.queryBy<MeteringInvoiceState>(fullCriteria) }, 20, 1000, mockNet)
      val meteringInvoiceState = meteringInvoiceStatesPage.states.single().state

      assert(meteringInvoiceState.data.meteringInvoiceProperties.meteredTransactionId == transactionId)
      assert(meteringInvoiceState.data.owner == daoNodeParty)
      assert(meteringInvoiceState.data.meteringInvoiceProperties.meteringState == meteringState)
    }
  }

  private fun checkVaultForMeteringInvoiceSplits(expectedMeteringInvoice: MeteringInvoiceState, node: StartedMockNode, meteringSplitState: MeteringSplitState, mockNet: MockNetwork) {
    node.transaction {

      val transactionId = expectedMeteringInvoice.meteringInvoiceProperties.meteredTransactionId

      val expression = MeteringInvoiceSplitSchemaV1.PersistentMeteringInvoiceSplit::meteringSplitState.equal(meteringSplitState.name)
      val customCriteria = QueryCriteria.VaultCustomQueryCriteria(expression)
      val generalCriteria = QueryCriteria.VaultQueryCriteria(Vault.StateStatus.ALL)
      val fullCriteria = generalCriteria.and(customCriteria)

      val meteringInvoiceSplitStatesPage = runAndRetryWithMockNetworkRefresh({ node.services.vaultService.queryBy<MeteringInvoiceSplitState>(fullCriteria) }, 20, 1000, mockNet)
      val meteringInvoiceSplitState = meteringInvoiceSplitStatesPage.states.single().state

      assert(meteringInvoiceSplitState.data.meteringInvoiceSplitProperties.meteredTransactionId == transactionId)
      assert(meteringInvoiceSplitState.data.meteringInvoiceSplitProperties.meteringSplitState == meteringSplitState)
    }
  }

  private fun createTokensAndLoadAccounts() {

    val sector1BankNodeLedger = LedgerApiImpl(TempHackedAppServiceHubImpl(sector1BankNode), { mockNet.runNetwork() })
    val sector2BankNodeLedger = LedgerApiImpl(TempHackedAppServiceHubImpl(sector2BankNode), { mockNet.runNetwork() })
    val meteringNotaryLedger = LedgerApiImpl(TempHackedAppServiceHubImpl(meteringNotary), { mockNet.runNetwork() })
    val daoNodeLedger = LedgerApiImpl(TempHackedAppServiceHubImpl(daoNode), { mockNet.runNetwork() })
    val guardianNotaryLedger = LedgerApiImpl(TempHackedAppServiceHubImpl(guardianNotary), { mockNet.runNetwork() })

    mockNet.runNetwork()

    testTokenXTS = daoNodeLedger.createTokenType(testCurrencyXTS, 2, meteringNotaryX500Name).getOrThrow()

    mockNet.runNetwork()

    val sector1BankAccountResult = sector1BankNodeLedger.createAccount(sector1BankAccount1, meteringNotaryX500Name).getOrThrow()
    assert(sector1BankAccountResult.address.accountId == sector1BankAccount1)
    val meteringNotaryAccountResult = meteringNotaryLedger.createAccount(meteringNotaryAccount1, meteringNotaryX500Name).getOrThrow()
    assert(meteringNotaryAccountResult.address.accountId == meteringNotaryAccount1)
    val daoAccountHoldingAccountResult = daoNodeLedger.createAccount(daoHoldingAccount, meteringNotaryX500Name).getOrThrow()
    assert(daoAccountHoldingAccountResult.address.accountId == daoHoldingAccount)
    val daoFoundationAccountAccountResult = daoNodeLedger.createAccount(daoFoundationAccount, meteringNotaryX500Name).getOrThrow()
    assert(daoFoundationAccountAccountResult.address.accountId == daoFoundationAccount)

    //Need to create two matching accounts for Guardian Notary, One on Dao, One on the Guardian Notary
    //This is because funds are temporarily stored on the Dao before switching notary
    val guardianNotaryAccountResult = guardianNotaryLedger.createAccount(guardianNotaryAccount1, meteringNotaryX500Name).getOrThrow()
    assert(guardianNotaryAccountResult.address.accountId == guardianNotaryAccount1)
    val guardianNotaryDaoAccountResult = daoNodeLedger.createAccount(guardianNotaryAccount1, meteringNotaryX500Name).getOrThrow()
    assert(guardianNotaryDaoAccountResult.address.accountId == guardianNotaryAccount1)

    val sectorBankAccount1Result = sector2BankNodeLedger.createAccount(sector2BankAccount1, meteringNotaryX500Name).getOrThrow()
    assert(sectorBankAccount1Result.address.accountId == sector2BankAccount1)

    val amount = "500.00"

    daoNodeLedger.issueToken(daoHoldingAccount, amount, testCurrencyXTS, "issuance to Dao", guardianNotaryX500Name).getOrThrow()

    mockNet.runNetwork()

    daoNodeLedger.transferAccountToAccount("100",
      testCurrencyXTS,
      "${WellKnownTagCategories.DGL_ID}:$daoHoldingAccount@${daoNode.info.legalIdentities.first().name}",
      "$sector1BankAccount1@${sector1BankNode.info.legalIdentities.first().name}",
      "transfer",
      guardianNotaryX500Name).getOrThrow()

    mockNet.runNetwork()

    daoNodeLedger.transferAccountToAccount("100",
      testCurrencyXTS,
      "${WellKnownTagCategories.DGL_ID}:$daoHoldingAccount@${daoNode.info.legalIdentities.first().name}",
      "$sector2BankAccount1@${sector2BankNode.info.legalIdentities.first().name}",
      "transfer",
      guardianNotaryX500Name).getOrThrow()

    mockNet.runNetwork()

    val sector1BankBalance = sector1BankNodeLedger.balanceForAccount(sector1BankAccount1).getOrThrow()
    val sector2BankBalance = sector2BankNodeLedger.balanceForAccount(sector2BankAccount1).getOrThrow()
    val daoRemaininfBalance = daoNodeLedger.balanceForAccount(daoHoldingAccount).getOrThrow()

    mockNet.runNetwork()

    assert(sector1BankBalance.first().quantity == 10000L)
    assert(sector2BankBalance.first().quantity == 10000L)
    assert(daoRemaininfBalance.first().quantity == 30000L)
  }

  private fun createAccountsAndDoSomeTransfers() : String {

    log.info("Creating Accounts for DGL Transfers")

    val sector1BankNodeLedger = LedgerApiImpl(TempHackedAppServiceHubImpl(sector1BankNode), { mockNet.runNetwork() })
    val sector2BankNodeLedger = LedgerApiImpl(TempHackedAppServiceHubImpl(sector2BankNode), { mockNet.runNetwork() })
    val daoNodeLedger = LedgerApiImpl(TempHackedAppServiceHubImpl(daoNode), { mockNet.runNetwork() })

    val sector1BankAccountResult = sector1BankNodeLedger.createAccount(sector1BankAccount2, meteringNotaryX500Name).getOrThrow()
    assert(sector1BankAccountResult.address.accountId == sector1BankAccount2)

    val sector2BankAccountResult = sector2BankNodeLedger.createAccount(sector2BankAccount2, meteringNotaryX500Name).getOrThrow()
    assert(sector2BankAccountResult.address.accountId == sector2BankAccount2)

    val daoFundAccountResult = daoNodeLedger.createAccount(daoIssuingAccount, meteringNotaryX500Name).getOrThrow()
    assert(daoFundAccountResult.address.accountId == daoIssuingAccount)

    val initialDaoAmount = "250.00"
    val transfer1ToSector1Bank = "50.00"
    val transfer2ToSector1Bank = "50.00"
    val transfer3ToSector1Bank = "50.00"
    val transferToSector2Bank = "150.00"
    val transferBackToSector1Bank = "125.00"

    daoNodeLedger.issueToken(daoIssuingAccount, initialDaoAmount, testCurrencyXTS, "initial Issuance", meteringNotaryX500Name).getOrThrow()

    mockNet.runNetwork()

    daoNodeLedger.transferAccountToAccount(transfer1ToSector1Bank,
      testTokenXTS.descriptor.uri,
      "${WellKnownTagCategories.DGL_ID}:$daoIssuingAccount@${daoNode.info.legalIdentities.first().name}",
      "$sector1BankAccount2@${sector1BankNode.info.legalIdentities.first().name}",
      "transfer",
      meteringNotaryX500Name).getOrThrow()

    mockNet.runNetwork()

    daoNodeLedger.transferAccountToAccount(transfer2ToSector1Bank,
      testTokenXTS.descriptor.uri,
      "${WellKnownTagCategories.DGL_ID}:$daoIssuingAccount@${daoNode.info.legalIdentities.first().name}",
      "$sector1BankAccount2@${sector1BankNode.info.legalIdentities.first().name}",
      "transfer",
      meteringNotaryX500Name).getOrThrow()

    mockNet.runNetwork()

    daoNodeLedger.transferAccountToAccount(transfer3ToSector1Bank,
      testTokenXTS.descriptor.uri,
      "${WellKnownTagCategories.DGL_ID}:$daoIssuingAccount@${daoNode.info.legalIdentities.first().name}",
      "$sector1BankAccount2@${sector1BankNode.info.legalIdentities.first().name}",
      "transfer",
      meteringNotaryX500Name).getOrThrow()

    mockNet.runNetwork()

    //Lets check the balances
    val sector1BankBalance1 = sector1BankNodeLedger.balanceForAccount(sector1BankAccount2).getOrThrow()
    val sector2BankBalance1 = sector2BankNodeLedger.balanceForAccount(sector2BankAccount2).getOrThrow()
    val daoRemainingBalance1 = daoNodeLedger.balanceForAccount(daoIssuingAccount).getOrThrow()

    assert(sector1BankBalance1.first().quantity == 15000L)
    assert(sector2BankBalance1.size ==0)
    assert(daoRemainingBalance1.first().quantity == 10000L)

   sector1BankNodeLedger.transferAccountToAccount(transferToSector2Bank,
      testTokenXTS.descriptor.uri,
      "$sector1BankAccount2",
      "$sector2BankAccount2@${sector2BankNode.info.legalIdentities.first().name}",
      "transfer II",
      meteringNotaryX500Name).getOrThrow()

    mockNet.runNetwork()

    //Lets check the balances
    val sector1BankBalance2 = sector1BankNodeLedger.balanceForAccount(sector1BankAccount2).getOrThrow()
    val sector2BankBalance2 = sector2BankNodeLedger.balanceForAccount(sector2BankAccount2).getOrThrow()
    val daoRemainingBalance2 = daoNodeLedger.balanceForAccount(daoIssuingAccount).getOrThrow()

    mockNet.runNetwork()

    assert(sector1BankBalance2.size ==0)
    assert(sector2BankBalance2.first().quantity == 15000L)
    assert(daoRemainingBalance2.first().quantity == 10000L)

    mockNet.runNetwork()

    //Transfer back to sector 1 bank - so that we get two inputs with the same transaction Id and Party
    val sector2BankTransferResult = sector2BankNodeLedger.transferAccountToAccount(transferBackToSector1Bank,
      testTokenXTS.descriptor.uri,
      "$sector2BankAccount2",
      "$sector1BankAccount2@${sector1BankNode.info.legalIdentities.first().name}",
      "transfer back",
      meteringNotaryX500Name).getOrThrow()

    mockNet.runNetwork()

    //Lets check the balances
    val sector1BankBalance3 = sector1BankNodeLedger.balanceForAccount(sector1BankAccount2).getOrThrow()
    val sector2BankBalance3 = sector2BankNodeLedger.balanceForAccount(sector2BankAccount2).getOrThrow()
    val daoRemainingBalance3 = daoNodeLedger.balanceForAccount(daoIssuingAccount).getOrThrow()

    mockNet.runNetwork()

    assert(sector1BankBalance3.first().quantity == 12500L)
    assert(sector2BankBalance3.first().quantity ==  2500L)
    assert(daoRemainingBalance3.first().quantity == 10000L)
    return sector2BankTransferResult.toString()
  }

  @After
  fun tearDown() {
    mockNet.stopNodes()
  }
}
