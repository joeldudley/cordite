/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.testutils

import io.bluebank.braid.corda.BraidConfig
import io.cordite.commons.utils.contextLogger
import io.cordite.commons.utils.jackson.CorditeJacksonInit
import io.cordite.dao.DaoApiImpl
import io.cordite.dgl.corda.impl.LedgerApiImpl
import io.cordite.metering.api.impl.MeteringServiceImpl
import net.corda.core.node.AppServiceHub
import net.corda.core.node.services.CordaService
import net.corda.core.serialization.SingletonSerializeAsToken

@CordaService
class MeteringTestBraidServer(serviceHub: AppServiceHub) : SingletonSerializeAsToken() {

  companion object {
    init {
      CorditeJacksonInit.init()
    }

    private val log = contextLogger()
  }

  private val org = serviceHub.myInfo.legalIdentities.first().name.organisation.replace(" ", "")
  private val portProperty = "braid.$org.port"

  init {
    val port = getBraidPort()
    when {
      port > 0 -> {
        log.info("starting $org braid on port $port")
        BraidConfig().withPort(port)
          .withService("dao", DaoApiImpl(serviceHub))
          .withService("meterer", MeteringServiceImpl(serviceHub))
          .withService("ledger", LedgerApiImpl(serviceHub))
          .bootstrapBraid(serviceHub)
      }
      else -> log.info("no port defined for $portProperty. not starting braid")
    }
  }

  private fun getBraidPort(): Int {
    return System.getProperty(portProperty)?.toInt() ?: 0
  }
}